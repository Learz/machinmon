import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.badlogic.gdx.utils.SerializationException;

import ca.kitaiweb.fairy.utils.PlayerLoader;
import ca.kitaiweb.fairy.utils.PlayerLoader.APIResponseException;

@RunWith(GdxTestRunner.class)
public class PlayerLoaderTest {

	@Test
	public void okResponseShouldPass() throws APIResponseException, NullPointerException, SerializationException {
		String string = "{\"response\":{},\"status\":{\"success\":{\"code\":200,\"status\":\"OK\"}}}";
		assertNotNull(PlayerLoader.parseJson(string));
	}
	
	@Test (expected = APIResponseException.class)
	public void errorResponseShouldNotPass() throws APIResponseException, NullPointerException, SerializationException {
		String string = "{\"status\":{\"error\":{\"code\":400,\"status\":\"Bad Request\"}}}";
		PlayerLoader.parseJson(string);
		fail("parseJson() with an error status should throw an error.");
	}

	@Test (expected = NullPointerException.class)
	public void nullStringCannotBeParsed() throws APIResponseException, NullPointerException, SerializationException {
		PlayerLoader.parseJson(null);
		fail("parseJson(null) should throw an error.");
	}

	@Test (expected = SerializationException.class)
	public void invalidJsonCannotBeParsed() throws APIResponseException, NullPointerException, SerializationException {
		String invalid = "{\"stats\"success\"code\":200,,,,\"stat.us\"::\"OK\"}}}";
		PlayerLoader.parseJson(invalid);
		fail("parseJson(invalid) should throw an error.");
	}
	
}
