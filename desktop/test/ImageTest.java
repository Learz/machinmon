import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;

import ca.kitaiweb.fairy.components.Image;

@RunWith(GdxTestRunner.class)
public class ImageTest {
	
	@Test
	public void testImageShouldWork() {
		new Image();
	}

	@Test
	public void testImagePixmapInvalidPixmapShouldReturnEmptyImage() {
		Image tester = new Image(new Pixmap(0, 0, Format.RGB888));
		assertEquals(0, tester.getImageHeight(), 0);
	}

	@Test
	public void testImageStringInvalidFileShouldReturnEmptyImage() {
		Image tester = new Image("thisshouldthrowan.error");
		assertEquals(0, tester.getImageHeight(), 0);
	}

	@Test
	public void testImageStringCorruptedFileShouldReturnEmptyImage() {
		Image tester = new Image("corrupted.png");
		assertEquals(0, tester.getImageHeight(), 0);
	}

	@Test (expected = NumberFormatException.class)
	public void testToNinePatchIntIntIntIntNegativeIntsShouldThrowError() {
		new Image().toNinePatch(-1, -3, 4, 0);
		fail("new Image().toNinePatch(-1, -3, 4, 0) should throw an error.");
	}

}
