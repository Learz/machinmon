package ca.kitaiweb.fairy.desktop;

import java.util.Random;

import ca.kitaiweb.fairy.utils.DataProvider;

public class RandomDataProvider implements DataProvider {
	
	private Random random;
	
	public RandomDataProvider() {
		random = new Random();
	}

	@Override
	public int getNextInt() {
		return random.nextInt();
	}

	@Override
	public int getNextInt(int limit) {
		return random.nextInt(limit);
	}

}
