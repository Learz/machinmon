package ca.kitaiweb.fairy.desktop;

import ca.kitaiweb.fairy.utils.DataProvider;

public class FixedDataProvider implements DataProvider {

	@Override
	public int getNextInt() {
		return 0;
	}

	@Override
	public int getNextInt(int limit) {
		return 0;
	}

}
