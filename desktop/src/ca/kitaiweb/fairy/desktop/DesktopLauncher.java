package ca.kitaiweb.fairy.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import ca.kitaiweb.fairy.FairyGame;
import ca.kitaiweb.fairy.components.Localizer;
import ca.kitaiweb.fairy.utils.DataProvider;

/**
 * The desktop launcher for the libGDX application.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & Fran�ois Allard
 */
public class DesktopLauncher {
	
	public static void main (String[] arg) {        
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 320;
		config.height = 480;
		Localizer localizer = new NullLocalizer();
		DataProvider randomizer = new FixedDataProvider();
		new LwjglApplication(new FairyGame(localizer, randomizer, true), config);
	}
}
