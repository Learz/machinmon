package ca.kitaiweb.fairy.desktop;

import ca.kitaiweb.fairy.components.Localizer;

public class NullLocalizer extends Localizer {

	public NullLocalizer() {
		setLat(45.8802909);
		setLon(-72.4842824);
	}
	
}
