package ca.kitaiweb.fairy.utils;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import ca.kitaiweb.fairy.models.Attack;
import ca.kitaiweb.fairy.models.Encounter;
import ca.kitaiweb.fairy.models.Fairy;
import ca.kitaiweb.fairy.models.FairyAttack;
import ca.kitaiweb.fairy.models.Item;
import ca.kitaiweb.fairy.models.User;
import ca.kitaiweb.fairy.models.UserFairy;
import ca.kitaiweb.fairy.models.UserItem;

import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.SerializationException;

/**
 * Player informations loader and generic Json parser
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class PlayerLoader {
	
	/**
	 * Type of exception this class can throw
	 */
	public static class APIResponseException extends IOException {

		private static final long serialVersionUID = -4311614057274255245L;
		
		private String code;

		public APIResponseException(String code, String message) {
			super(message);
			this.code = code;
		}
		
		public APIResponseException(String code, String message, Throwable cause) {
			super(message, cause);
			this.code = code;
		}
		
		public String getCode() {
			return code;
		}		
		
	}
	
	/**
	 * Parses the string into a JsonValue
	 * 
	 * @param response The Json string
	 * @return The parsed String as a JsonValue object
	 * @throws APIResponseException If the json is an error message
	 */
	public static JsonValue parseJson(String response) throws APIResponseException, NullPointerException, SerializationException {
		JsonValue jsonResponse = new JsonReader().parse(response);
		if (jsonResponse.getChild("status").getInt("code") == 200) {
			return jsonResponse.get("response");
		} else {
			throw new APIResponseException(jsonResponse.getChild("status").getString("code"), jsonResponse.getChild("status").getString("status"));
		}
	}

	/**
	 * Get the player informations from a pre-parsed JsonValue
	 * @param jsonResponse The JsonValue containing the player informations
	 * @param password The player SHA-256 encoded password 
	 * @return The user object
	 */
	public static User getPlayer(JsonValue jsonResponse, String password) {
		User user = new User(	jsonResponse.getInt("id"), 
								jsonResponse.getString("username"), 
								password,
								jsonResponse.getString("email"), 
								jsonResponse.getInt("primary"));
		user.setParty(getParty(jsonResponse.get("party")));
		user.setInventory(getInventory(jsonResponse.get("inventory")));
		return user;
	}
	
	/**
	 * Get the party of the user from a pre-parsed JsonValue
	 * @param jsonResponse The JsonValue containing the player's party
	 * @return A list of the user's fairies
	 */
	public static List<UserFairy> getParty(JsonValue jsonResponse) {
		List<UserFairy> party = new LinkedList<UserFairy>();
		for (JsonValue value : jsonResponse.iterator()) {
			party.add(getUserFairy(value));
		}
		return party;
	}
	
	/**
	 * Get an user's fairy from a pre-parsed JsonValue
	 * @param jsonResponse The JsonValue containing the userFairy
	 * @return The userFairy object
	 */
	public static UserFairy getUserFairy(JsonValue jsonResponse) {
		UserFairy userFairy = new UserFairy(getFairy(jsonResponse.get("fairy")), 
											jsonResponse.getString("name"), 
											jsonResponse.getInt("level"), 
											jsonResponse.getInt("health"));
		return userFairy;
	}
	
	/**
	 * Get the fairy informations from a pre-parsed JsonValue
	 * @param jsonResponse The JsonValue containing the fairy
	 * @return The fairy object
	 */
	public static Fairy getFairy(JsonValue jsonResponse) {
		Fairy fairy = new Fairy(jsonResponse.getInt("id"), 
								jsonResponse.getString("name"), 
								jsonResponse.getString("description"),
								jsonResponse.getInt("max_health"),
								jsonResponse.getFloat("defense"));
		List<FairyAttack> attacks = new LinkedList<FairyAttack>();
		for (JsonValue value2 : jsonResponse.get("attacks")) {
			JsonValue attackRoot = value2.get("attack");
			Attack attack = new Attack(	attackRoot.getInt("id"), 
										attackRoot.getString("name"), 
										attackRoot.getString("description"), 
										attackRoot.getInt("damage"), 
										attackRoot.getInt("type"));
			FairyAttack fairyAttack = new FairyAttack(attack, value2.getInt("unlock_level"));
			attacks.add(fairyAttack);
		}
		fairy.setAttacks(attacks);
		return fairy;
	}
	
	/**
	 * Get the inventory from a pre-parsed JsonValue
	 * @param jsonResponse The JsonValue containing the inventory
	 * @return A list of UserItems objects
	 */
	public static List<UserItem> getInventory(JsonValue jsonResponse) {
		List<UserItem> inventory = new LinkedList<UserItem>();
		for (JsonValue value : jsonResponse.iterator()) {
			JsonValue itemRoot = value.get("item");
			Item item = new Item(	itemRoot.getInt("id"), 
									itemRoot.getString("name"), 
									itemRoot.getString("description"));
			UserItem userItem = new UserItem(item, value.getInt("quantity"));
			inventory.add(userItem);
		}
		return inventory;
	}
	
	/**
	 * Get the encounters from a pre-parsed JsonValue
	 * @param jsonResponse The JsonValue containing the encounters
	 * @return A list of encounters objects
	 */
	public static List<Encounter> getEncounters(JsonValue jsonResponse) {
		List<Encounter> encounters = new LinkedList<Encounter>();
		for (JsonValue value : jsonResponse.iterator()) {
			Encounter encounter = new Encounter(getFairy(value.get("fairy")), 
												value.getFloat("lon"), 
												value.getFloat("lat"), 
												value.getString("date"));
			encounters.add(encounter);
		}
		return encounters;
	}
	
}
