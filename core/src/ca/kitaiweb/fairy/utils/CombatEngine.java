package ca.kitaiweb.fairy.utils;

import ca.kitaiweb.fairy.FairyGame;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.Net.HttpResponseListener;

import ca.kitaiweb.fairy.models.User;
import ca.kitaiweb.fairy.models.UserFairy;
import ca.kitaiweb.fairy.utils.APIRequest.APIRequestType;
import ca.kitaiweb.fairy.utils.PlayerLoader.APIResponseException;

// TODO: Force a fairy swap on the death of the current fairy.
// TODO: Make the user lose the game only when all the party has been defeated
// NOTE: You lost the game too.

/**
 * The combat engine
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class CombatEngine {
	
	public enum CombatStatus {
		ACTIONSELECT,
		ATTACKSELECT,
		ITEMSELECT,
		FAIRYSELECT,
		FLEEING,
		ATTACKING,
		USINGITEM,	// MUCH USING ITEM
		SWAPING, 	// SUCH SWAPING
		WAITING,	// VERY WAITING
		LOST,		// SO LOST
		WON			// WOW
	}
	
	/*
	░░░░░░░░░▄░░░░░░░░░░░░░░▄░░░░
	░░░░░░░░▌▒█░░░░░░░░░░░▄▀▒▌░░░
	░░░░░░░░▌▒▒█░░░░░░░░▄▀▒▒▒▐░░░
	░░░░░░░▐▄▀▒▒▀▀▀▀▄▄▄▀▒▒▒▒▒▐░░░
	░░░░░▄▄▀▒░▒▒▒▒▒▒▒▒▒█▒▒▄█▒▐░░░
	░░░▄▀▒▒▒░░░▒▒▒░░░▒▒▒▀██▀▒▌░░░
	░░▐▒▒▒▄▄▒▒▒▒░░░▒▒▒▒▒▒▒▀▄▒▒▌░░
	░░▌░░▌█▀▒▒▒▒▒▄▀█▄▒▒▒▒▒▒▒█▒▐░░
	░▐░░░▒▒▒▒▒▒▒▒▌██▀▒▒░░░▒▒▒▀▄▌░
	░▌░▒▄██▄▒▒▒▒▒▒▒▒▒░░░░░░▒▒▒▒▌░
	▀▒▀▐▄█▄█▌▄░▀▒▒░░░░░░░░░░▒▒▒▐░
	▐▒▒▐▀▐▀▒░▄▄▒▄▒▒▒▒▒▒░▒░▒░▒▒▒▒▌
	▐▒▒▒▀▀▄▄▒▒▒▄▒▒▒▒▒▒▒▒░▒░▒░▒▒▐░
	░▌▒▒▒▒▒▒▀▀▀▒▒▒▒▒▒░▒░▒░▒░▒▒▒▌░
	░▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒░▒░▒░▒▒▄▒▒▐░░
	░░▀▄▒▒▒▒▒▒▒▒▒▒▒░▒░▒░▒▄▒▒▒▒▌░░
	░░░░▀▄▒▒▒▒▒▒▒▒▒▒▄▄▄▀▒▒▒▒▄▀░░░
	░░░░░░▀▄▄▄▄▄▄▀▀▀▒▒▒▒▒▄▄▀░░░░░
	░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▀▀░░░░░░░░
	*/
	
	private CombatStatus combatStatus;
	private User player;
	private String capturedFairyName;
	public UserFairy playerFairy;
	public UserFairy opponentFairy;
	private int selectedFairy;
	private int round;
	private int lastPlayerHit;
	private int lastOpponentHit;
	private FairyGame game;

	/**
	 * Initialize the Combat Engine
	 * @param player The player's user
	 * @param opponent The opponent's user
	 */
	public CombatEngine(FairyGame game, User opponent) {
		round = 0;
		selectedFairy = 0;
		this.game = game;
		this.player = this.game.getUser();
		playerFairy = this.player.getParty().get(this.player.getPrimary());
		opponentFairy = opponent.getParty().get(opponent.getPrimary());
		setStatus(CombatStatus.ACTIONSELECT);
	}
	
	/**
	 * Set the combat status
	 * @param status The combat status enum
	 */
	public synchronized void setStatus(CombatStatus status) {
		combatStatus = status;
		System.out.println(status);
	}
	
	/**
	 * Get the combat status
	 * @return The combat status enum
	 */
	public synchronized CombatStatus getStatus() {
		return combatStatus;
	}
	
	/**
	 * Changes the {@link UserFairy playerFairy} to a specified index
	 * @param id The User's fairy id in his party
	 */
	public boolean setPlayerFairy(int id){
		if (player.getParty().get(id).getHealth() > 0) {
			playerFairy = player.getParty().get(id);
			selectedFairy = id;
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Get the last hit from the player
	 * @return Damage from the hit
	 */
	public int getLastPlayerHit(){
		return lastPlayerHit;
	}
	
	/**
	 * Get the last hit from the opponent
	 * @return Damage from the hit
	 */
	public int getLastOpponentHit(){
		return lastOpponentHit;
	}

	/**
	 * Attack the opponent if the attack exists and then go to defense
	 * @param id The attack id
	 * @return The combat status at the end of the attack
	 */
	public synchronized CombatStatus attack(int id) {
		lastPlayerHit = 0;
		if (getStatus() == CombatStatus.ATTACKING) {
			if (id < playerFairy.getFairy().getAttacks().size()) {
				int damage = playerFairy.getFairy().getAttacks().get(id).getAttack().getDamage();
				if (damage > 0) {
					lastPlayerHit = (int) (damage / opponentFairy.getFairy().getDefense());
					float health = opponentFairy.getHealth() - lastPlayerHit;
					opponentFairy.setHealth((int)health);
					if (opponentFairy.getHealth() == 0) {
						setStatus(CombatStatus.WON);
						game.combatScreen.endCombatDialog("You won!");
						return getStatus();
					}
				} else if (damage < 0) {
					lastPlayerHit = damage;
					playerFairy.setHealth(playerFairy.getHealth() - damage);
				}
				setStatus(CombatStatus.WAITING);
			}
		}
		return getStatus();
	}
	
	/**
	 * Get attacked by the opponent then go to action select
	 * @return The combat status at the end of the defense
	 */
	public synchronized CombatStatus defend() {
		lastOpponentHit = 0;
		if (getStatus() == CombatStatus.WAITING) {
			if (opponentFairy.getFairy().getAttacks().size() > 0) {
				int choice = game.randomizer.getNextInt(opponentFairy.getFairy().getAttacks().size());
				int damage = opponentFairy.getFairy().getAttacks().get(choice).getAttack().getDamage();
				if(damage > 0){
					lastOpponentHit = (int) (damage / playerFairy.getFairy().getDefense());
					float health = playerFairy.getHealth() - lastOpponentHit;
					playerFairy.setHealth((int)health);
					if (playerFairy.getHealth() <= 0) {
						playerFairy.setHealth(0);
						setStatus(CombatStatus.LOST);
						game.combatScreen.endCombatDialog("You lost...");
						return getStatus();
					}
				}else if (damage < 0){
					lastOpponentHit = damage;
					opponentFairy.setHealth(opponentFairy.getHealth() - damage);
				}
			}
			round++;
			setStatus(CombatStatus.ACTIONSELECT);
		}
		return getStatus();
	}
	
	/**
	 * Checks if the opponent has been captured
	 * @return true if the opponent is captured
	 */
	public boolean hasCaptured() {
		if (game.randomizer.getNextInt(opponentFairy.getHealth()) <= opponentFairy.getFairy().getMaxHealth() / 8) {
			return true;
		} else {
			setStatus(CombatStatus.WAITING);
			defend();
			return false;
		}
	}
	
	/**
	 * Adds the captured fairy to the database
	 * @return False if the user's party is already full
	 */
	public void capture(){
		Map<String, String> fairy_info = new HashMap<String, String>();
		fairy_info.put("fairy_id", String.valueOf(opponentFairy.getFairy().getId()));
		fairy_info.put("level", String.valueOf(opponentFairy.getLevel()));
		if(!capturedFairyName.isEmpty())
			fairy_info.put("name", capturedFairyName);
		APIRequest request = new APIRequest("http://protofairy.tk/api/user/party", APIRequestType.INSERT);
		request.setCredentials(game.getUser().getUsername(), game.getUser().getPassword());
		request.addParameters(fairy_info);
		request.sendRequest(new HttpResponseListener() {
			@Override
			public void handleHttpResponse(HttpResponse httpResponse) {
				System.out.println("Success");
				try {
					PlayerLoader.parseJson(httpResponse.getResultAsString());
				} catch (APIResponseException e) {
					System.err.println(e.getCode()+" -> "+e.getMessage());
					switch(Integer.parseInt(e.getCode())) {
					case 409:
						game.combatScreen.showParty(CombatStatus.SWAPING);
						break;
					case 201:
						setStatus(CombatStatus.WON);
						game.combatScreen.endCombatDialog("You captured "+opponentFairy.getName()+"!");
						break;
					}
				}
			}
			public void failed(Throwable t) {System.out.println("Fail");}
			public void cancelled() {}
		});
		
	}
	
	/**
	 * Set the captured fairy's name
	 * @param name The personalized name
	 */
	public void setCapturedFairyName(String name){
		capturedFairyName = name;
	}
	
	/**
	 * Ge the captured fairy's name
	 * @return The personalized name
	 */
	public String getCapturedFairyName(){
		return capturedFairyName;
	}
	
	/**
	 * Removes a fairy from the database
	 */
	public void swapFairy(int id){
		APIRequest request = new APIRequest("http://protofairy.tk/api/user/party/"+id, APIRequestType.DELETE);
		request.setCredentials(game.getUser().getUsername(), game.getUser().getPassword());
		request.sendRequest(new HttpResponseListener() {
			@Override
			public void handleHttpResponse(HttpResponse httpResponse) {
				System.out.println("Successfully removed fairy");
				capture();
				setStatus(CombatStatus.WON);
			}
			public void failed(Throwable t) {System.out.println("Fairy removal failed");}
			public void cancelled() {}
		});
	}
	
	/**
	 * Checks if the player has fled
	 * @return true if the player successfully fled
	 */
	public boolean hasFled() {
		if (game.randomizer.getNextInt(opponentFairy.getHealth()) <= opponentFairy.getFairy().getMaxHealth() / 3) {
			return true;
		} else {
			setStatus(CombatStatus.WAITING);
			defend();
			return false;
		}
	}
	
	/**
	 * Saves the current fairy's information and sends it to the server
	 */
	public void saveFairy() {
		int additionalXP = (round*5 > 50) ? 50 : round*5;
		UserFairy fairy = playerFairy;
		Map<String, String> fairy_info = new HashMap<String, String>();
		fairy_info.put("fairy_id", String.valueOf(selectedFairy));
		//fairy_info.put("damage", String.valueOf(fairy.getFairy().getMaxHealth() - fairy.getHealth()));
		fairy_info.put("damage", "0");
		fairy_info.put("name", String.valueOf(fairy.getName()));
		if(combatStatus == CombatStatus.WON){
			fairy_info.put("xp", String.valueOf(opponentFairy.getFairy().getMaxHealth()+additionalXP));
		}
		APIRequest request = new APIRequest("http://protofairy.tk/api/user/party", APIRequestType.UPDATE);
		request.setCredentials(game.getUser().getUsername(), game.getUser().getPassword());
		request.addParameters(fairy_info);
		request.sendRequest(new HttpResponseListener() {
			@Override
			public void handleHttpResponse(HttpResponse httpResponse) {
				try {
					PlayerLoader.parseJson(httpResponse.getResultAsString());
					System.out.println("Success");
				} catch (APIResponseException e) {
					System.err.println(e.getCode()+" -> "+e.getMessage());
				}
			}
			public void failed(Throwable t) {System.out.println("Fail");}
			public void cancelled() {}
		});
	}
	
}
