package ca.kitaiweb.fairy.utils;

public interface DataProvider {

	int getNextInt();
	int getNextInt(int limit);
	
}
