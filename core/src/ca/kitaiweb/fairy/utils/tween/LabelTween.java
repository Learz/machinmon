package ca.kitaiweb.fairy.utils.tween;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

import aurelienribon.tweenengine.TweenAccessor;

/**
 * The label class for tweening.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class LabelTween implements TweenAccessor<Label> {
	public static final int X = 1;
	public static final int Y = 2;
	public static final int XY = 3;
	public static final int OPACITY = 4;

	public int getValues(Label target, int tweenType, float[] returnValues) {
		switch (tweenType) {
			case X: returnValues[0] = target.getX(); return 1;
			case Y: returnValues[0] = target.getY(); return 1;
			case XY:
				returnValues[0] = target.getX();
				returnValues[1] = target.getY();
			return 2;
			case OPACITY: returnValues[0] = target.getColor().a; return 1;
			default: assert false; return 0;
		}
	}
	public void setValues(Label target, int tweenType, float[] newValues) {
			switch (tweenType) {
			case X: target.setPosition(newValues[0], target.getY()); break;
			case Y: target.setPosition(target.getX(), newValues[0]); break;
			case XY:
				target.setPosition(newValues[0], newValues[1]);
			break;
			case OPACITY: 
				target.setColor(target.getColor().r, target.getColor().g, target.getColor().b, newValues[0]); 
			break;
			default: assert false; break;
		}
	}
}
