package ca.kitaiweb.fairy.utils.tween;

import com.badlogic.gdx.graphics.g2d.Sprite;

import aurelienribon.tweenengine.TweenAccessor;

/**
 * The sprite movement class for tweening.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class SpriteTween implements TweenAccessor<Sprite> {
	public static final int X = 1;
	public static final int Y = 2;
	public static final int XY = 3;

	public int getValues(Sprite target, int tweenType, float[] returnValues) {
		switch (tweenType) {
			case X: returnValues[0] = target.getX(); return 1;
			case Y: returnValues[0] = target.getY(); return 1;
			case XY:
			returnValues[0] = target.getX();
			returnValues[1] = target.getY();
			return 2;
			default: assert false; return 0;
		}
	}
	public void setValues(Sprite target, int tweenType, float[] newValues) {
			switch (tweenType) {
			case X: target.setX(newValues[0]); break;
			case Y: target.setY(newValues[1]); break;
			case XY:
			target.setX(newValues[0]);
			target.setY(newValues[1]);
		break;
		default: assert false; break;
		}
	}
}