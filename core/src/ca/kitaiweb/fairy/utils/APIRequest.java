package ca.kitaiweb.fairy.utils;

import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.HttpRequest;
import com.badlogic.gdx.Net.HttpResponseListener;
import com.badlogic.gdx.net.HttpParametersUtils;

/**
 * The http request class.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class APIRequest {


	/**
	 *	The type of HTTP request for the API
	 */
	public enum APIRequestType {
		/** HTTP GET request equivalent */
		SELECT	("GET"),
		/** HTTP POST request equivalent */
		INSERT	("POST"),
		/** HTTP PUT request equivalent */
		UPDATE	("PUT"),
		/** HTTP DELETE request equivalent */
		DELETE	("DELETE");
		
		private String HttpRequestType;
		
		private APIRequestType(String type) {
			HttpRequestType = type;
		}
		
		public String getHttpRequestType() {
			return HttpRequestType;
		}
	}
	
	public APIRequestType requestType;
	private HttpRequest httpRequest;
	private String httpMethod;
	
	/**
	 * Initialize the APIRequest
	 * 
	 * @param url The url of the request
	 * @param type The type of the request
	 */
	public APIRequest (String url, APIRequestType type) {
		httpMethod = type.getHttpRequestType();
		httpRequest = new HttpRequest(httpMethod);
		httpRequest.setUrl(url);
		// The api requires "x-www-form-urlencoded" to work properly like a RESTful api.
		httpRequest.setHeader("content-type", "application/x-www-form-urlencoded");
	}
	
	/**
	 * Sends the request the the API
	 * 
	 * @param listener The HTTP response listener
	 */
	public void sendRequest(HttpResponseListener listener) {
		/*System.out.println(httpRequest.getUrl());
		System.out.println(httpRequest.getMethod());
		System.out.println(httpRequest.getHeaders());
		System.out.println(httpRequest.getContent());*/
		Gdx.net.sendHttpRequest(this.httpRequest, listener);
	}
	
	/**
	 * Add the parameters for a PUT or POST request
	 * @param parameters 
	 */
	public void addParameters(Map<String, String> parameters){
		httpRequest.setContent(HttpParametersUtils.convertHttpParameters(parameters));
	}
	
	/**
	 * Sets the username and password for header authentication
	 * 
	 * @param user The username
	 * @param pass The password
	 */
	public void setCredentials(String user, String pass) {
		httpRequest.setHeader("username", user);
		httpRequest.setHeader("password", pass);
	}
	
	/**
	 * Disposes the request after usage
	 */
	public void dispose() {
		httpRequest = null;
	}
	
}
