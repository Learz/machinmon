package ca.kitaiweb.fairy.utils;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

/**
 * The Json object factory class.
 * 
 * @deprecated Use {@link APIRequest} with {@link PlayerLoader} instead
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class JsonObjectFactory<APIObject> {
	
	private final Class<APIObject> type;

	/**
	 * Initialize the factory
	 * @param type
	 * @throws InstantiationException
	 */
	public JsonObjectFactory(Class<APIObject> type) throws InstantiationException {
		try {
			Class<?> temp = Class.forName("ca.kitaiweb.fairy.models.APIObject");
			if (!temp.isAssignableFrom(type)) {
				throw new InstantiationException("Class is not assignable to APIObject.");
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		this.type = type;
	}
	
	/**
	 * Get the Objects
	 * @param response
	 * @return
	 */
	public List<APIObject> getAPIObjects(String response) {
		Json json = new Json();
		JsonValue root = new JsonReader().parse(response);
		List<APIObject> apiObjects = new ArrayList<APIObject>();
		if (root.child().child() != null) {
			for (JsonValue value : root.iterator()) {
				try {
					APIObject apiObject = json.fromJson(type, value.toString());
					apiObjects.add(apiObject);
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
			}
		} else {
			try {
				APIObject apiObject = json.fromJson(type, root.toString());
				apiObjects.add(apiObject);
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		}
		return apiObjects;
	}
}
