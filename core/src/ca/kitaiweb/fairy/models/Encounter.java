package ca.kitaiweb.fairy.models;

/**
 * A user's fairy encounter
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class Encounter {
	
	private Fairy fairy;
	private float lon;
	private float lat;
	private String date;
	
	/**
	 * Initializes the encounter
	 * 
	 * @param fairy The fairy encountered
	 * @param lon The horizontal position where the fairy was found
	 * @param lat The vertical position where the fairy was found
	 * @param date When the fairy was found
	 */
	public Encounter(Fairy fairy, float lon, float lat, String date) {
		this.fairy = fairy;
		this.lon = lon;
		this.lat = lat;
		this.date = date;
	}
	
	/**
	 * Get the fairy encountered
	 * @return The fairy encountered
	 */
	public Fairy getFairy() {
		return fairy;
	}
	
	/**
	 * Get the longitude where the fairy was encountered
	 * @return The longitude
	 */
	public float getLon() {
		return lon;
	}
	
	/**
	 * Get the latitude where the fairy was encountered
	 * @return The latitude
	 */
	public float getLat() {
		return lat;
	}
	
	/**
	 * Get the date when the fairy was encountered
	 * @return The date
	 */
	public String getDate() {
		return date;
	}

}
