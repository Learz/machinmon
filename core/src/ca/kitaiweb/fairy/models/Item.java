package ca.kitaiweb.fairy.models;

/**
 * A general item
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class Item {
	
	private int id;
	private String name;
	private String description;
	
	/**
	 * Initializes the item
	 * @param id The item's id
	 * @param name The item's name
	 * @param description the item's description
	 */
	public Item(int id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}
	
	/**
	 * Get the item's id
	 * @return The item's id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Get the item's name
	 * @return The item's name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Get the item's description
	 * @return The item's description
	 */
	public String getDescription() {
		return description;
	}
	
}
