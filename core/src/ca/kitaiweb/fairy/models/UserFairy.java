package ca.kitaiweb.fairy.models;

/**
 * An user's fairy
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class UserFairy {
	
	private Fairy fairy;
	private String name;
	private int level;
	private int health;
	
	/**
	 * Initializes the userFairy
	 * @param fairy The {@link Fairy}
	 * @param name The personalized fairy name
	 * @param level The fairy's level
	 * @param health The fairy's health
	 */
	public UserFairy(Fairy fairy, String name, int level, int health) {
		this.fairy = fairy;
		this.name = name;
		this.level = level;
		setHealth(health);
	}
	
	/**
	 * Get the {@link Fairy}
	 * @return The {@link Fairy}
	 */
	public Fairy getFairy() {
		return fairy;
	}
	
	/**
	 * Get the personalized name
	 * @return The name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Get the fairy's level
	 * @return The fairy's level
	 */
	public int getLevel() {
		return level;
	}
	
	/**
	 * Get the fairy's  current health
	 * @return The fairy's current health
	 */
	public int getHealth() {
		return health;
	}

	/**
	 * Set the fairy's health adjusting between 0 and maxHealth
	 * @param health The fairy's health
	 */
	public void setHealth(int health) {
		if(health <= fairy.getMaxHealth() && health >= 0)
			this.health = health;
		else if (health > fairy.getMaxHealth())
			this.health = fairy.getMaxHealth();
		else if (health < 0)
			this.health = 0;
	}

}
