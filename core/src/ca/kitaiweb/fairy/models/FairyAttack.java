package ca.kitaiweb.fairy.models;

/**
 * A fairy's attack
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class FairyAttack {
	
	private Attack attack;
	private int unlockLevel;
	
	/**
	 * Initializes the attack and the level it unlocks for the fairy
	 * 
	 * @param attack The fairy attack
	 * @param unlockLevel The level it is unlocked
	 */
	public FairyAttack(Attack attack, int unlockLevel) {
		this.attack = attack;
		this.unlockLevel = unlockLevel;
	}
	
	/**
	 * Get the attack
	 * @return The attack
	 */
	public Attack getAttack() {
		return attack;
	}
	
	/**
	 * Get the level when the attack is unlocked
	 * @return The level
	 */
	public int getUnlockLevel() {
		return unlockLevel;
	}

}
