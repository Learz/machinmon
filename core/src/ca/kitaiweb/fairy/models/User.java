package ca.kitaiweb.fairy.models;

import java.util.List;

/**
 * The user's informations
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class User {
	
	private int id;
	private String username;
	/** The SHA-256 encoded user's password*/
	final private String password;
	private String email;
	private int primary;
	private List<UserFairy> party;
	private List<UserItem> inventory;
	
	/**
	 * Initializes the user
	 * 
	 * @param id The user id
	 * @param username The user's username
	 * @param password The user's {@link #password}
	 * @param email The user's e-mail adress
	 * @param primary The user's primary fairy
	 */
	public User(int id, String username, String password, String email, int primary) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.email = email;
		this.primary = primary;
	}
	
	/**
	 * Get the user's id
	 * @return The user's id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Get the user's username
	 * @return The username
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * Get the user's {@link #password}
	 * @return The user's {@link #password}
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * Get the user's e-mail address
	 * @return the user's e-mail address
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * Get the user's primary fairy index
	 * @return The user's primary fairy index
	 */
	public int getPrimary() {
		return primary;
	}
	
	/**
	 * Sets the user's party
	 * @param party List of {@link UserFairy}
	 */
	public void setParty(List<UserFairy> party) {
		this.party = party;
	}
	
	/**
	 * Sets the user's inventory
	 * @param inventory List of {@link UserItem}
	 */
	public void setInventory(List<UserItem> inventory) {
		this.inventory = inventory;
	}
	
	/**
	 * Get the user's party
	 * @return The user's list of {@link UserFairy}
	 */
	public List<UserFairy> getParty() {
		return party;
	}
	
	/**
	 * Get the user's inventory
	 * @return The user's list of {@link UserItem}
	 */
	public List<UserItem> getInventory() {
		return inventory;
	}
	
}
