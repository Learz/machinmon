package ca.kitaiweb.fairy.models;

/**
 * The user's items
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class UserItem {

	private Item item;
	private int quantity;
	
	/**
	 * Initializes an user's item
	 * @param item The item
	 * @param quantity The quantity of this item
	 */
	public UserItem(Item item, int quantity) {
		this.item = item;
		this.quantity = quantity;
	}
	
	/**
	 * Get the item
	 * @return The item
	 */
	public Item getItem() {
		return item;
	}
	
	/**
	 * Get the quantity of the item
	 * @return The quantity of the item
	 */
	public int getQuantity() {
		return quantity;
	}
	
}
