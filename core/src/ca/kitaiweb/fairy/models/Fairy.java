package ca.kitaiweb.fairy.models;

import java.util.List;

/**
 * A fairy's general informations.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class Fairy {
	
	private int id;
	private String name;
	private String description;
	private int maxHealth;
	private List<FairyAttack> attacks;
	private float defense;
	
	/**
	 * Initializes the fairy
	 * @param id The fairy id
	 * @param name The fairy name
	 * @param description The fairy Description
	 * @param maxHealth The fairy max health
	 * @param defense the fairy resistance to damage
	 */
	public Fairy(int id, String name, String description, int maxHealth, float defense) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.maxHealth = maxHealth;
		this.defense = defense;
	}
	
	/**
	 * Get the fairy id
	 * @return The fairy id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Get the fairy name
	 * @return The fairy name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Get the fairy description
	 * @return The fairy description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Get the fairy max health
	 * @return The fairy max health
	 */
	public int getMaxHealth() {
		return maxHealth;
	}
	
	/**
	 * Get the fairy defense (resistance to damage)
	 * @return The fairy resistance
	 */
	public float getDefense() {
		return defense;
	}
	
	/**
	 * Set the fairy's attacks
	 * @param attacks The {@link FairyAttack fairy's attacks} list
	 */
	public void setAttacks(List<FairyAttack> attacks) {
		this.attacks = attacks;
	}
	
	/**
	 * Get the fairy's attacks
	 * @return The {@link FairyAttack fairy's attacks} list
	 */
	public List<FairyAttack> getAttacks() {
		return attacks;
	}
	
}
