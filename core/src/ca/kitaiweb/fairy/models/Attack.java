package ca.kitaiweb.fairy.models;

/**
 * A fairy's attack
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class Attack {
	
	/**
	 *	The type of damage made from the attack
	 */
	public enum DamageType {
		/** Normal Damage(0) */
		NORMAL (0), 
		/** Fire Damage(1) */
		FIRE (1), 
		/** Water Damage(2) */
		WATER (2);
		
		private int value;
		
		DamageType(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return value;
		}
		
		public static DamageType getType(int value) {
			DamageType type = NORMAL;
			switch (value) {
			case 0:
				type = NORMAL;
				break;
			case 1:
				type = FIRE;
				break;
			case 2:
				type = WATER;
				break;
			}
			return type;
		}
		
	}
	
	private int id;
	private String name;
	private String description;
	private int damage;
	private DamageType type;
	
	/**
	 * Initializes the attack
	 * @param id The attack id
	 * @param name The attack name
	 * @param description The description of the attack
	 * @param damage The damage caused by the attack, negative equal healing
	 * @param type The {@link DamageType} of the attack
	 */
	public Attack(int id, String name, String description, int damage, int type) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.damage = damage;
		this.type = DamageType.getType(type);
	}
	
	/**
	 * Get the attack id
	 * @return The attack id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Get the attack name
	 * @return The attack name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Get the attack description
	 * @return The attack description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Get the attack damage
	 * @return The attack damage
	 */
	public int getDamage() {
		return damage;
	}
	
	/**
	 * Get the {@link DamageType}
	 * @return The {@link DamageType}
	 */
	public DamageType getType() {
		return type;
	}
}
