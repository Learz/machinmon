package ca.kitaiweb.fairy.components;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Scaling;

/**
 * The image class.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class Image extends com.badlogic.gdx.scenes.scene2d.ui.Image {
	
	/**
	 * Creates an empty image
	 */
	public Image() {
		this("mt.png");
	}
	
	/**
	 * Creates an image from a pixmap
	 * 
	 * @param pixmap The GDX pixmap
	 */
	public Image(Pixmap pixmap) {
		this(pixmap, TextureFilter.Linear, TextureFilter.Linear);
	}
	
	/**
	 * Creates an image from a path
	 * 
	 * @param path The internal path of the image
	 */
	public Image(String path) {
		this(path, TextureFilter.Linear, TextureFilter.Linear);
	}
	
	/**
	 * Creates an image from a path with the desired filter
	 * 
	 * @param path The internal path of the image
	 * @param all The GDX TextureFilter
	 */
	public Image(String path, TextureFilter all) {
		this(path, all, all);
	}
	
	/**
	 * Creates an image from a GDX pixmap with the desired filter
	 * 
	 * @param pixmap The GDX pixmap
	 * @param all The GDX TextureFilter
	 */
	public Image(Pixmap pixmap, TextureFilter all) {
		this(pixmap, all, all);
	}
	
	/**
	 * Creates an image from a path with the desired distinct filters
	 * 
	 * @param path The internal path of the image
	 * @param min The GDX TextureFilter for minified images
	 * @param mag The GDX TextureFilter for magnified images
	 */
	public Image(String path, TextureFilter min, TextureFilter mag) {
		super(loadTexture(path, min, mag));
		this.setScaling(Scaling.fit);
	}
	
	/**
	 * Creates an image from a pixmap with the desired distinct filters
	 * 
	 * @param path The GDX pixmap
	 * @param min The GDX TextureFilter for minified images
	 * @param mag The GDX TextureFilter for magnified images
	 */
	public Image(Pixmap pixmap, TextureFilter min, TextureFilter mag) {
		super(loadTexture(pixmap, min, mag));
		this.setScaling(Scaling.fit);
	}
	
	/**
	 * The texture loader for paths
	 * 
	 * @param path The internal patch
	 * @param min the minified filter
	 * @param mag the magnified filter
	 * @return the image texture
	 */
	private static Texture loadTexture(String path, TextureFilter min, TextureFilter mag) {
		try {
			Texture texture = new Texture(path);
			texture.setFilter(min, mag);
			return texture;
		} catch (Exception e) {
			Texture texture = new Texture("mt.png");
			texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
			return texture;
		}
	}
	
	/**
	 * The texture loader for pixmaps
	 * 
	 * @param path The GDX pixmap
	 * @param min the minified filter
	 * @param mag the magnified filter
	 * @return the image texture
	 */
	private static Texture loadTexture(Pixmap pixmap, TextureFilter min, TextureFilter mag) {
		try {
			Texture texture = new Texture(pixmap);
			texture.setFilter(min, mag);
			return texture;
		} catch (Exception e) {
			Texture texture = new Texture("mt.png");
			texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
			return texture;
		}
	}
	
	/**
	 * Converts the image to a ninepatch
	 * 
	 * @return the ninepatch as a drawable
	 */
	public NinePatchDrawable toNinePatch() {
		return toNinePatch(6,6,6,6);
	}
	
	/**
	 * Converts the image to a configurable ninepatch
	 * 
	 * @param left The left margin
	 * @param right The right margin
	 * @param top The top margin
	 * @param bottom The bottom margin
	 * @return the ninepatch as a drawable
	 * @throws NumberFormatException If left, right, top or bottom are negative
	 */
	public NinePatchDrawable toNinePatch(int left, int right, int top, int bottom) throws NumberFormatException {
		if (left >= 0 && right >= 0 && top >= 0 && bottom >= 0)
			return new NinePatchDrawable(new NinePatch(((TextureRegionDrawable) this.getDrawable()).getRegion().getTexture(), left, right, top, bottom));
		else
			throw new NumberFormatException();
	}
	
	/**
	 * Converts the image to a TextureRegion
	 * 
	 * @return the TextureRegion as a drawable
	 */
	public TextureRegionDrawable toTextureRegion() {
		return (TextureRegionDrawable) this.getDrawable();
	}

}
