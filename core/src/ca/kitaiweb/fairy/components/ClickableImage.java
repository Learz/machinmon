package ca.kitaiweb.fairy.components;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * A clickable image changing the texture region on click.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class ClickableImage extends com.badlogic.gdx.scenes.scene2d.ui.Button {

	/**
	 * Initialize the button with an image background separated or not into two equal parts horizontally.
	 * 
	 * @param image The path for the image
	 * @param split If the image should be split into two parts
	 */
	public ClickableImage(String image, boolean split){
		super(generateStyle(image, split));
	}
	
	/**
	 * Generates the ButtonStyle.
	 * 
	 * @param image The path for the image
	 */
	private static ButtonStyle generateStyle(String image, boolean split) {
		Texture tex = new Texture(image);
		int splitPoint = split ? tex.getWidth() / 2 : tex.getWidth();
		TextureRegion[] images = TextureRegion.split(tex, splitPoint, tex.getHeight())[0];
		TextureRegionDrawable neutral = new TextureRegionDrawable(images[0]);
		TextureRegionDrawable clicked;
		if (images.length > 1) {
			clicked = new TextureRegionDrawable(images[1]);
		} else {
			clicked = neutral;
		}
		return new ButtonStyle(neutral, clicked, neutral);
	}


}
