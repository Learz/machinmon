package ca.kitaiweb.fairy.components;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

/**
 * A custom font singleton.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class Font {
	private static Font instance = null;
	private static final int[] sizes = new int[] {32, 24, 16, 12, 10, 8, 6};
	public Map<Integer, BitmapFont> font;
	
	protected Font() {
		this(TextureFilter.Linear, TextureFilter.Linear);
	}
	
	/**
	 * Initialize the font once with a size of 32.
	 */
	protected Font(TextureFilter min, TextureFilter mag) {
		font = new HashMap<Integer, BitmapFont>();
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("OpenSans-Regular.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.minFilter = min;
		parameter.magFilter = mag;
		for (int i = 0; i < sizes.length; i++) {
			parameter.size = sizes[i];
			font.put(sizes[i], generator.generateFont(parameter));
		}
		generator.dispose();
	}

	/**
	 * Getter for the font object to only initialize it once.
	 * 
	 * @return The instance of the font object
	 */
	public static Font getInstance() {
		if (instance == null) {
			instance = new Font();
		}
		return instance;
	}
}

