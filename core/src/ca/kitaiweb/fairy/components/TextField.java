package ca.kitaiweb.fairy.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * A customized text field with a preset font.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class TextField extends com.badlogic.gdx.scenes.scene2d.ui.TextField {

	/**
	 * Initialize the text field with a text.
	 * 
	 * @param text The text to show in the text field
	 * @param fontSize The font size for the text
	 */
	public TextField(String text){
		super(text, generateStyle(16));
	}

	/**
	 * Initialize the text field with a text.
	 * 
	 * @param text The text to show in the text field
	 * @param fontSize The font size for the text
	 */
	public TextField(String text, int fontSize){
		super(text, generateStyle(fontSize));
	}
	
	/**
	 * Generates the TextFieldStyle.
	 */
	private static TextFieldStyle generateStyle(int fontSize){
		BitmapFont font = Font.getInstance().font.get(fontSize);
		NinePatchDrawable bg = new Image("textField.png").toNinePatch(2,0,0,2);
		TextureRegionDrawable cur = new Image("textField_cursor.png", TextureFilter.Nearest).toTextureRegion();
		return new TextFieldStyle(font, Color.WHITE, cur, cur, bg);
	}

}
