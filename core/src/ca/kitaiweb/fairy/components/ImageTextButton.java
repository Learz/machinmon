package ca.kitaiweb.fairy.components;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

/**
 * The item class for the CustomList.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class ImageTextButton extends Button {
	
	private Label text;
	private Image image;
	
	/**
	 * Initializes a list item with a text.
	 * 
	 * @param text The text
	 */
	public ImageTextButton(String text) {
		super(generateStyle("default"));
		this.add(this.text = new Label(text));
	}
	
	/**
	 * Initializes a list item with a text and a font size.
	 * 
	 * @param text The text
	 * @param fontSize The font size for the text
	 */
	public ImageTextButton(String text, int fontSize) {
		super(generateStyle("default"));
		this.add(this.text = new Label(text, fontSize));
	}
	
	/**
	 * Initializes a list item with an icon and a text.
	 * 
	 * @param iconTex The icon texture
	 * @param text The text
	 */
	public ImageTextButton(String iconTex, String text) {
		this(iconTex, text, false);
	}
	
	/**
	 * Initializes a list item with an icon and a text.
	 * 
	 * @param iconTex The icon texture
	 * @param text The text
	 * @param vertical If the text should be under the icon
	 */
	public ImageTextButton(String iconTex, String text, boolean vertical) {
		super(generateStyle("default"));
		this.image = new Image(iconTex+".png");
		if (vertical) {
			this.add(this.image).center();
			this.row();
			this.add(this.text = new Label(text)).center();
		} else {
			this.add(this.image).left();
			this.add(this.text = new Label(text));
		}
	}
	
	/**
	 * Initializes a list item with an icon, a text and a font size.
	 * 
	 * @param iconTex The icon texture
	 * @param text The text
	 * @param fontSize The font size for the text
	 */
	public ImageTextButton(String iconTex, String text, int fontSize) {
		this(iconTex, text, fontSize, false);
	}
	
	/**
	 * Initializes a list item with an icon, a text and a font size.
	 * 
	 * @param iconTex The icon texture
	 * @param text The text
	 * @param fontSize The font size for the text
	 */
	public ImageTextButton(String iconTex, String text, int fontSize, boolean vertical) {
		super(generateStyle("default"));
		this.image = new Image(iconTex+".png");
		if (vertical) {
			this.add(this.image).center();
			this.row();
			this.add(this.text = new Label(text, fontSize)).center();
		} else {
			this.add(this.image).left();
			this.add(this.text = new Label(text, fontSize));
		}
	}
	
	/**
	 * Set the button's text
	 * @param text
	 */
	public void setText(String text){
		this.text.setText(text);
	}
	
	/**
	 * Generates the default button style
	 * 
	 * @param image the ninepatch image for the button
	 * @return ButtonStyle
	 */
	private static ButtonStyle generateStyle(String image) {
		NinePatchDrawable patch = new Image("buttons/"+image+".png").toNinePatch();
		NinePatchDrawable patchClick = new Image("buttons/"+image+"_clicked.png").toNinePatch();
		return new ButtonStyle(patch, patchClick, patch);
	}
	
}
