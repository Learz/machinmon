package ca.kitaiweb.fairy.components;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

/**
 * A custom list made from a table
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class CustomList extends Table{
	
	List<ImageTextButton> items = new ArrayList<ImageTextButton>();
	
	/**
	 * Initialize the list with default settings
	 */
	public CustomList() {
		this.setFillParent(true);
		this.bottom();
	}
	
	/**
	 * Add a ListItem with events to the list
	 * Must be used inside show()
	 * @param item
	 */
	public void addItem(ImageTextButton item, EventListener listener){
		addItem(item);
		item.addListener(listener);
	}
	
	/**
	 * Add a ListItem to the list
	 * @param item
	 */
	public void addItem(ImageTextButton item){
		items.add(item);
		this.add(items.get(items.size()-1)).pad(5).fill();
		this.row();
	}
	
	//TODO: Remove row with actor.
	public void removeItem(int index){
		this.removeActor(items.get(index));
	}
}
