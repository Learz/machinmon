package ca.kitaiweb.fairy.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

/**
 * A customized label with a preset font.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class Label extends com.badlogic.gdx.scenes.scene2d.ui.Label {

	/**
	 * Initialize the label with a text.
	 * 
	 * @param text The text to show in the label
	 */
	public Label(String text) {
		this(text, 16);
	}

	/**
	 * Initialize the label with a text.
	 * 
	 * @param text The text to show in the label
	 * @param fontSize The font size for the text
	 */
	public Label(String text, int fontSize) {
		super(text, generateStyle(fontSize));
	}
	
	/**
	 * Generates the default LabelStyle.
	 */
	private static LabelStyle generateStyle(int fontSize) {
		BitmapFont font = Font.getInstance().font.get(fontSize);
		return new LabelStyle(font, Color.WHITE);
	}

	@Override
	public void draw (Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
	}
}
