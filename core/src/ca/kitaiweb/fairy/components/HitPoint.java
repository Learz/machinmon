package ca.kitaiweb.fairy.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

/**
 * The hitpoint object to show damage and health regen on fairies
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud and François Allard
 */
public class HitPoint extends Label{
	
	//TODO: Tweaking
    private Actor playerActor;
    private Actor opponentActor;
    private Vector2 playerPosition;
    private Vector2 opponentPosition;
    private Color playerColor;
    private Color opponentColor;
    
    /**
     * Initializes the hitpoint for a single actor
     * @param playerActor Used for the origin of the hitpoint
     */
	public HitPoint(Actor playerActor) {
		super("", 24);
		this.playerActor = playerActor;
	}
	
	/**
	 * Initializes the hitpoint for two actors
	 * @param playerActor Used for the origin of the player hitpoint
	 * @param opponentActor Used for the origin of the opponent hitpoint
	 */
	public HitPoint(Actor playerActor, Actor opponentActor) {
		super("", 24);
		this.playerActor = playerActor;
		this.opponentActor = opponentActor;
		playerColor = new Color();
		opponentColor = new Color();
	}
	
	/**
	 * Shows the hitpoint with for the player
	 * @param playerDamage The amount of damage
	 * @param end Used for multithreading purposes
	 */
	public void showPlayerHit(int playerDamage, Runnable end){
		playerPosition = playerActor.localToStageCoordinates(new Vector2(playerActor.getWidth(),playerActor.getHeight()/2));
		this.setText(String.valueOf(-playerDamage));
		
		if(playerDamage > 0) {
			playerColor.set(255, 0, 0, 1);
			playerPosition.set(opponentActor.localToStageCoordinates(new Vector2(opponentActor.getWidth(),opponentActor.getHeight()/2)));
		} else if (playerDamage < 0) {
			playerColor.set(0, 255, 0, 1);
		} else {
			playerColor.set(0,0,0,0);
		}
		
		this.addAction(
			Actions.sequence(
				Actions.moveTo(playerPosition.x, playerPosition.y),
				Actions.color(playerColor),
				Actions.parallel(
					Actions.alpha(0, 1.0f, Interpolation.pow2), 
					Actions.moveBy(0, 50, 1.0f, Interpolation.pow2)),
				Actions.run(end)
		));
				
	}
	
	/**
	 * Shows the hitpoint for the opponent
	 * @param opponentDamage The amount of damage
	 */
	public void showOpponentHit(int opponentDamage) {
		opponentPosition = opponentActor.localToStageCoordinates(new Vector2(opponentActor.getWidth(),opponentActor.getHeight()/2));
		this.setText(String.valueOf(-opponentDamage));
		
		if(opponentDamage > 0) {
			opponentColor.set(255, 0, 0, 1);
			opponentPosition.set(playerActor.localToStageCoordinates(new Vector2(playerActor.getWidth(),playerActor.getHeight()/2)));
		} else if (opponentDamage < 0) {
			opponentColor.set(0, 255, 0, 1);
		} else {
			opponentColor.set(0,0,0,0);
		}

		this.addAction(
			Actions.sequence(
				Actions.moveTo(opponentPosition.x, opponentPosition.y),
				Actions.color(opponentColor),
				Actions.parallel(
					Actions.alpha(0, 1.0f, Interpolation.pow2), 
					Actions.moveBy(0, 50, 1.0f, Interpolation.pow2))
		));
	}
	
}
