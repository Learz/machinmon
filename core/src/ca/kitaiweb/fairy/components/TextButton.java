package ca.kitaiweb.fairy.components;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

/**
 * A customized button with an NinePatch in the background and a text on top of it.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class TextButton extends com.badlogic.gdx.scenes.scene2d.ui.TextButton{

	/**
	 * Initialize the button with a text.
	 * 
	 * @param text The text to show on top of the button
	 */
	public TextButton(String text){
		this(text, "default");
	}	

	/**
	 * Initialize the button with a text and a font size.
	 * 
	 * @param text The text to show on top of the button
	 * @param fontSize The font size for the text
	 */
	public TextButton(String text, int fontSize){
		this(text, "default", fontSize);
	}	

	/**
	 * Initialize the button with a text and a NinePatch background with 6px borders.
	 * 
	 * @param text The text to show on top of the button
	 * @param image The path for the background image
	 */
	public TextButton(String text, String image){
		this(text, image, 6, 6, 6, 6);
	}

	/**
	 * Initialize the button with a text, a font size and a NinePatch background with 6px borders.
	 * 
	 * @param text The text to show on top of the button
	 * @param image The path for the background image
	 * @param fontSize The font size for the text
	 */
	public TextButton(String text, String image, int fontSize){
		this(text, image, fontSize, 6, 6, 6, 6);
	}

	/**
	 * Initialize the button with a text and a NinePatch background with custom borders.
	 * 
	 * @param text The text to show on top of the button
	 * @param image The path for the background image
	 * @param left The left border width in pixels
	 * @param right The right border width in pixels
	 * @param up The top border height in pixels
	 * @param down The bottom border height in pixels
	 */
	public TextButton(String text, String image, int left, int right, int top, int bottom){
		super(text, generateStyle(image, 16, left, right, top, bottom));
	}

	/**
	 * Initialize the button with a text, a font size and a NinePatch background with custom borders.
	 * 
	 * @param text The text to show on top of the button
	 * @param image The path for the background image
	 * @param fontSize The font size for the text
	 * @param left The left border width in pixels
	 * @param right The right border width in pixels
	 * @param up The top border height in pixels
	 * @param down The bottom border height in pixels
	 */
	public TextButton(String text, String image, int fontSize, int left, int right, int top, int bottom){
		super(text, generateStyle(image, fontSize, left, right, top, bottom));
	}
	
	/**
	 * Generates the TextButtonStyle.
	 * 
	 * @param text The text to show on top of the button
	 * @param image The path for the background image
	 * @param fontSize The font size for the text
	 * @param left The left border width in pixels
	 * @param right The right border width in pixels
	 * @param up The top border height in pixels
	 * @param down The bottom border height in pixels
	 */
	private static TextButtonStyle generateStyle(String image, int fontSize, int left, int right, int top, int bottom){
		BitmapFont font = Font.getInstance().font.get(fontSize);
		NinePatchDrawable patch = new Image("buttons/"+image+".png").toNinePatch(left, right, top, bottom);
		NinePatchDrawable patchClick = new Image("buttons/"+image+"_clicked.png").toNinePatch(left, right, top, bottom);
		return new TextButtonStyle(patch, patchClick, patch, font);
	}


}
