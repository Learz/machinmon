package ca.kitaiweb.fairy.components;

import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * A customized button with an NinePatch in the background and an icon in the center.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class ImageButton extends com.badlogic.gdx.scenes.scene2d.ui.ImageButton{
	
	/**
	 * Initialize the button with an icon and the default background.
	 * 
	 * @param icon The path for the icon image
	 */
	public ImageButton(String icon){
		this(icon, "default");
	}	

	/**
	 * Initialize the button with an icon and a NinePatch background with 6px borders.
	 * 
	 * @param icon The path for the icon image
	 * @param image The path for the background image
	 */
	public ImageButton(String icon, String image){
		this(icon, image, 6, 6, 6, 6);
	}

	/**
	 * Initialize the button with an icon and a NinePatch background with custom borders.
	 * 
	 * @param icon The path for the icon image
	 * @param image The path for the background image
	 * @param left The left border width in pixels
	 * @param right The right border width in pixels
	 * @param up The top border height in pixels
	 * @param down The bottom border height in pixels
	 */
	public ImageButton(String iconTex, String image, int left, int right, int top, int bottom){
		super(generateStyle(iconTex,image,left,right,top,bottom));
	}
	
	/**
	 * Generates the ImageButtonStyle.
	 * 
	 * @param icon The path for the icon image
	 * @param image The path for the background image
	 * @param left The left border width in pixels
	 * @param right The right border width in pixels
	 * @param up The top border height in pixels
	 * @param down The bottom border height in pixels
	 */
	private static ImageButtonStyle generateStyle(String iconTex, String image, int left, int right, int top, int bottom){
		NinePatchDrawable patch = new Image("buttons/"+image+".png").toNinePatch(left,right,top,bottom);
		NinePatchDrawable patchClick = new Image("buttons/"+image+"_clicked.png").toNinePatch(left,right,top,bottom);
		TextureRegionDrawable icon = new Image(iconTex+".png").toTextureRegion();
		return new ImageButtonStyle(patch, patchClick, patch, icon, icon, icon);
	}

}
