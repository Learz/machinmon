package ca.kitaiweb.fairy.components;

import com.badlogic.gdx.Gdx;

/**
 * The position listener used in the game.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public abstract class Localizer {

	private double lon;
	private double lat;
	private double oldLon = 0;
	private double oldLat = 0;
	
    /**
     * Sets the longitude
     * @param _lon The longitude
     */
	public void setLon(double _lon) {
		lon = _lon;
	}
	
	/**
	 * Sets the latitude
	 * @param _lat The latitude
	 */
	public void setLat(double _lat) {
		lat = _lat;
	}
	
	/**
	 * Returns the longitude
	 * @return The longitude
	 */
	public double getLon() {
		return lon;
	}
	
	/**
	 * Returns the latitude
	 * @return The latitude
	 */
	public double getLat() {
		return lat;
	}

	/**
	 * Fires on application gaining focus
	 */
	public void onResume() {
		;
	}
	
	/**
	 * Fires on application losing focus
	 */
	public void onPause() {
		;
	}
	
	/**
	 * Requests an update of the location
	 */
	public void requestUpdate() {
		;
	}
	
	/**
	 * Checks if the user has moved significantly
	 */
	public boolean hasMoved() {
		double lat = this.lat / 100;
		double lon = this.lon / 100;
		double oldLat = this.oldLat / 100;
		double oldLon = this.oldLon / 100;
		Gdx.app.log("lat", String.valueOf(lat));
		Gdx.app.log("lon", String.valueOf(lon));
		Gdx.app.log("oldLat", String.valueOf(oldLat));
		Gdx.app.log("oldLon", String.valueOf(oldLon));
		
		if (lat != oldLat || lon != oldLon) {
			this.oldLat = this.lat;
			this.oldLon = this.lon;
			Gdx.app.log("moved", "true");
			return true;
		} else {
			Gdx.app.log("moved", "false");
			return false;
		}
	}
	
}
