package ca.kitaiweb.fairy.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

/**
 * A customized dialog box
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class Dialog extends com.badlogic.gdx.scenes.scene2d.ui.Dialog{

	public Dialog(String title) {
		super(title, GenerateStyle("dialog"));
		this.setMovable(false);
		this.pad(50).padTop(70).padBottom(10);
		this.getButtonTable().padTop(5);
	}

	private static WindowStyle GenerateStyle(String background){
		NinePatchDrawable dialog = new Image(background + ".png").toNinePatch(1,1,1,1);
		return new WindowStyle(Font.getInstance().font.get(16), Color.WHITE, dialog);
	}
}
