package ca.kitaiweb.fairy;

import java.util.HashMap;

import ca.kitaiweb.fairy.components.Label;
import ca.kitaiweb.fairy.components.Localizer;
import ca.kitaiweb.fairy.models.User;
import ca.kitaiweb.fairy.screens.*;
import ca.kitaiweb.fairy.utils.APIRequest;
import ca.kitaiweb.fairy.utils.APIRequest.APIRequestType;
import ca.kitaiweb.fairy.utils.DataProvider;
import ca.kitaiweb.fairy.utils.PlayerLoader.APIResponseException;
import ca.kitaiweb.fairy.utils.PlayerLoader;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.Net.HttpResponseListener;
import com.badlogic.gdx.Screen;

/**
 * The libGDX game main class.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class FairyGame extends Game {
	
	public LoginScreen loginScreen;
	public MapScreen mapScreen;
	public MenuScreen menuScreen;
	public PartyScreen partyScreen;
	public EncounterScreen encounterScreen;
	public DexScreen dexScreen;
	public FairyScreen fairyScreen;
	public CombatScreen combatScreen;
	public Localizer localizer;
	public DataProvider randomizer;
	private User user;
	public final static int SCREENWIDTH = 320;
	public final static int SCREENHEIGHT = 480;
	public static boolean DEBUG;
	
	/**
	 * Initializes the game
	 * 
	 * @param loc The localizer to use
	 */
	public FairyGame(Localizer loc, DataProvider rand, boolean debug) {
		super();
		randomizer = rand;
		localizer = loc;
		DEBUG = debug;
	}

	@Override
	public void create () {
		mapScreen = new MapScreen(this);
		loginScreen = new LoginScreen(this);
		menuScreen = new MenuScreen(this);
		partyScreen = new PartyScreen(this);
		encounterScreen = new EncounterScreen(this);
		dexScreen = new DexScreen(this);
		fairyScreen = new FairyScreen(this);
		combatScreen = new CombatScreen(this);
		setScreen(loginScreen);
	}
	
	@Override
	public void resume() {
		super.resume();
		localizer.onResume();
	}
	
	@Override
	public void pause() {
		super.pause();
		localizer.onPause();
	}

	@Override
    public void render() {
        super.render();
    }
	
	@Override
	public void dispose() {
		mapScreen.dispose();
		loginScreen.dispose();
		menuScreen.dispose();
		partyScreen.dispose();
		encounterScreen.dispose();
		dexScreen.dispose();
		fairyScreen.dispose();
		combatScreen.dispose();
	}
	
	/**
	 * Sets the next screen with tweening between the old and new screens
	 * 
	 * @param screen The new screen to display
	 * @param origin The direction in which to push the old and new screens
	 */
	public void setScreenWithTransition(Screen screen, int origin) {
	    ScreenTransition transition = new ScreenTransition(getScreen(), screen, this, origin);
	    setScreen(transition);
	}
	
	/**
	 * Sets the next screen with tweening between the old and new screens, and adds parameters to it
	 * 
	 * @param screen The new screen to display
	 * @param parameters Parameters to be passed on to the new screen
	 * @param origin The direction in which to push the old and new screens
	 */
	public void setScreenWithTransition(ParametrableScreen screen, HashMap<String, String> parameters, int origin) {
		screen.pushParam(parameters);
	    setScreenWithTransition(screen, origin);
	}
	
	/**
	 * Get the authenticated user
	 * 
	 * @return The authenticated user (can be null)
	 */
	public User getUser() {
		return user;
	}
	
	/**
	 * Authenticates the user on the API server, shows errors and changes screen
	 * 
	 * @param screen The screen to show if authentication succeeds
	 * @param origin The direction in which push the old and new screens
	 * @param username The username of the user to authenticate
	 * @param password The password of the user to authenticate
	 * @param errorField The label in which the error will be shown (if any)
	 */
	public void authenticateUser(final Screen screen, final int origin, String username, final String password, final Label errorField) {
		APIRequest apiRequest = new APIRequest("http://protofairy.tk/api/user/complete", APIRequestType.SELECT);
		apiRequest.setCredentials(username, password);
		apiRequest.sendRequest(new HttpResponseListener() {
			@Override
			public void handleHttpResponse(HttpResponse httpResponse) {
				try {
					user = PlayerLoader.getPlayer(PlayerLoader.parseJson(httpResponse.getResultAsString()), password);
					Gdx.app.postRunnable(new Runnable() {
						@Override
						public void run() {
							setScreenWithTransition(screen, origin);
						}
					});
				} catch (APIResponseException e) {
					System.err.println(e.getCode()+" -> "+e.getMessage());
					switch(Integer.parseInt(e.getCode())) {
					case 403:
						errorField.setText("Bad username or password.");
						break;
					case 503:
						errorField.setText("Service unavailable.");
						break;
					case 500:
						errorField.setText("Server error. Try again later.");
						break;
					}
				}
			}
			public void failed(Throwable t) {
				errorField.setText("Service unavailable.");
			}
			public void cancelled() {
				errorField.setText("Service unavailable.");
			}
		});
	}
	

	/**
	 * Authenticates the user on the API server
	 * 
	 * @param username The username of the user to authenticate
	 * @param password The password of the user to authenticate
	 */
	public void loadUser() {
		APIRequest apiRequest = new APIRequest("http://protofairy.tk/api/user/complete", APIRequestType.SELECT);
		apiRequest.setCredentials(getUser().getUsername(), getUser().getPassword());
		apiRequest.sendRequest(new HttpResponseListener() {
			@Override
			public void handleHttpResponse(HttpResponse httpResponse) {
				try {
					user = PlayerLoader.getPlayer(PlayerLoader.parseJson(httpResponse.getResultAsString()), getUser().getPassword());
				} catch (APIResponseException e) {
					System.err.println(e.getCode()+" -> "+e.getMessage());
				}
			}
			public void failed(Throwable t) {
			}
			public void cancelled() {
			}
		});
	}
}