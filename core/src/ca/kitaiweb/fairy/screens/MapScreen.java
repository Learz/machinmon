package ca.kitaiweb.fairy.screens;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import ca.kitaiweb.fairy.FairyGame;
import ca.kitaiweb.fairy.components.*;
import ca.kitaiweb.fairy.utils.APIRequest;
import ca.kitaiweb.fairy.utils.APIRequest.APIRequestType;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.Net.HttpResponseListener;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

/**
 * The navigation screen.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class MapScreen extends ParametrableScreen {

	private Table table;
	private Table table2;
	private ImageButton menu;
	private ImageButton dex;
	private ClickableImage refresh;
	private List<Integer> possibleFairies;
	private Image map;
	private APIRequest request;
	private int encounterCount = 10;
	
	/**
	 * Initializes the map screen
	 * 
	 * @param fairyGame The game instance in which to display the screen
	 */
	public MapScreen(FairyGame fairyGame) {
		super(fairyGame);
		possibleFairies = new LinkedList<Integer>();
		possibleFairies.add(1);
		possibleFairies.add(2);
		possibleFairies.add(3);
		encounterCount = game.randomizer.getNextInt(10) + 5;
		
		//monsters = new ArrayList<ClickableImage>();
		menu = new ImageButton("menu", "tab_left");
		dex = new ImageButton("dex", "tab_right");
		refresh = new ClickableImage("radar_dot.png", true);
		
		map = new Image();
	    stage.addActor(map);
	    
		table = new Table();
	    table.setFillParent(true);
	    
	    table.row();
		table.add(menu).expand().left();
		table.add(table2);
		table.add(dex).expand().right();
		table.setZIndex(90);
	    stage.addActor(table);
	    
	    table2 = new Table();
	    table2.setFillParent(true);
	    
	    table2.add(refresh).pad(5);
		table.setZIndex(100);
	    stage.addActor(table2);

		map.setDrawable(new Image(Gdx.files.getLocalStoragePath()+"world_map.png").getDrawable());
		map.setBounds(0, 0, map.getDrawable().getMinWidth(), map.getDrawable().getMinHeight());
	    
	    //ClickableImage monster = new ClickableImage("slime.png", false);
	    //monster.setX(200);
	    //monster.setY(200);
	    //monster.setWidth(50);
	    //monster.setHeight(50);
	    //monsters.add(monster);
	    
	    menu.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				game.setScreenWithTransition(game.menuScreen, ScreenTransition.LEFT);
			}
	    });
	    dex.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				game.setScreenWithTransition(game.dexScreen, ScreenTransition.RIGHT);
			}
	    });
	    refresh.addListener(new ChangeListener() {
	    	@Override
	    	public void changed(ChangeEvent event, Actor actor) {
	    		game.localizer.requestUpdate();
	    		if (game.localizer.hasMoved()) {
		    		request = new APIRequest("http://maps.googleapis.com/maps/api/staticmap?center="+game.localizer.getLat()+","+game.localizer.getLon()+"&zoom=17&size="+(int)Math.ceil(viewport.getWorldWidth())+"x"+(int)Math.ceil(viewport.getWorldHeight()), APIRequestType.SELECT);
		    		request.sendRequest(new HttpResponseListener() {
		    			@Override
		    			public void handleHttpResponse(HttpResponse httpResponse) {
		    	    		final byte[] rawImageBytes = httpResponse.getResult();
		    	    		Gdx.app.postRunnable(new Runnable() {
		    	    			@Override
		    	    			public void run () {
		    	    				Pixmap pixmap = new Pixmap(rawImageBytes, 0, rawImageBytes.length);
		    	    				map.setDrawable(new Image(pixmap).getDrawable());
		    	    				map.setBounds(0, 0, map.getDrawable().getMinWidth(), map.getDrawable().getMinHeight());
		    	    				FileHandle file = new FileHandle(Gdx.files.getLocalStoragePath()+"world_map.png");
		    	    				PixmapIO.writePNG(file, pixmap);
		    	    				pixmap.dispose();
		    	    			}
		    	    		});
		    			}
		    			public void failed(Throwable t) {}
		    			public void cancelled() {}
		    		});
	    			encounterCount--;
	    		} else if (FairyGame.DEBUG) {
	    			encounterCount--;
	    		}
	    	}
	    });
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	    stage.act(delta);
		stage.draw();
		if (encounterCount <= 0) {
			encounterCount = game.randomizer.getNextInt(10) + 5;
			HashMap<String, String> parameters = new HashMap<String, String>();
			parameters.put("fairy_id", String.valueOf(possibleFairies.get(game.randomizer.getNextInt(possibleFairies.size()))));
			game.setScreenWithTransition(game.combatScreen, parameters, ScreenTransition.TOP);
		}
	}
	
	@Override
	public void show() {
		/*int index = 0;
		for (final ClickableImage item : monsters) {
			final int i = index;
			item.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					HashMap<String, String> parameters = new HashMap<String, String>();
					parameters.put("id", String.valueOf(i));
					game.setScreenWithTransition(game.fairyScreen, parameters, ScreenTransition.TOP);
				}
			});
			stage.addActor(item);
			index++;
		}*/
		Gdx.input.setInputProcessor(stage);
	}
	
	@Override
	public void hide() {
	}
	
	@Override
	public void resize (int width, int height) {
	    stage.getViewport().update(width, height, true);
	}
	
	@Override
	public void dispose() {
	    stage.dispose();
	}
}
