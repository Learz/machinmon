package ca.kitaiweb.fairy.screens;

import ca.kitaiweb.fairy.FairyGame;
import ca.kitaiweb.fairy.components.Image;
import ca.kitaiweb.fairy.components.Label;
import ca.kitaiweb.fairy.components.TextButton;
import ca.kitaiweb.fairy.models.Encounter;
import ca.kitaiweb.fairy.utils.APIRequest;
import ca.kitaiweb.fairy.utils.PlayerLoader;
import ca.kitaiweb.fairy.utils.APIRequest.APIRequestType;
import ca.kitaiweb.fairy.utils.PlayerLoader.APIResponseException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.Net.HttpResponseListener;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

/**
 * The encounter description screen
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class EncounterScreen extends ParametrableScreen {

	private Table table;
	private TextButton btnQuit;
	private Label lblName;
	private Label lblDescription;
	private Label lblDate;
	private Image background;
	private APIRequest request;
	
	/**
	 * Initializes the encounter screen
	 * 
	 * @param fairyGame The game instance in which to display the screen
	 */
	public EncounterScreen(FairyGame fairyGame) {
		super(fairyGame);
		background = new Image("background.png");
		stage.addActor(background);
		lblName = new Label("", 32);
		lblDescription = new Label("", 24);
		lblDescription.setWrap(true);
		lblDate = new Label("", 24);
		lblDate.setAlignment(Align.center);
		btnQuit = new TextButton("Return", 24);
		
		table = new Table();
	    table.setFillParent(true);
	    
		table.add(lblName).expandY().top();
		table.row();
		table.add(lblDescription).fill().expand().top();
		table.row();
		table.add(lblDate).fill().expand().bottom();
		table.row();
		table.add(btnQuit).expandY().bottom();
		table.pad(10);
		
	    stage.addActor(table);
	    
	    btnQuit.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				game.setScreenWithTransition(game.dexScreen, ScreenTransition.BOTTOM);
			}
	    });
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	    stage.act(delta);
		stage.draw();
	}
	
	@Override
	public void show() {
		request = new APIRequest("http://protofairy.tk/api/user/encounters/"+getParam("id"), APIRequestType.SELECT);
		request.setCredentials(game.getUser().getUsername(), game.getUser().getPassword());
		request.sendRequest(new HttpResponseListener() {
			@Override
			public void handleHttpResponse(HttpResponse httpResponse) {
				final String result = httpResponse.getResultAsString();
				Gdx.app.postRunnable(new Runnable() {
					@Override
					public void run() {
						try {
							Encounter selection = PlayerLoader.getEncounters(PlayerLoader.parseJson(result)).get(0);
							lblName.setText(selection.getFairy().getId() + " - " + selection.getFairy().getName());
							lblDescription.setText(selection.getFairy().getDescription());
							lblDate.setText(selection.getDate());
						} catch (APIResponseException e) {
							System.err.println(e.getCode()+" -> "+e.getMessage());
						}
					}
				});
			}
			public void failed(Throwable t) {}
			public void cancelled() {}
		});
		Gdx.input.setInputProcessor(stage);
	}
	
	@Override
	public void hide() {
		lblName.setText("");
		lblDescription.setText("");
		lblDate.setText("");
	}
	
	@Override
	public void resize (int width, int height) {
	    stage.getViewport().update(width, height, true);
	}
	
	@Override
	public void dispose() {
	    stage.dispose();
	}
}
