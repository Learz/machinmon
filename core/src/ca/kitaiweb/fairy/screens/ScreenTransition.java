package ca.kitaiweb.fairy.screens;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;
import aurelienribon.tweenengine.equations.*;
import ca.kitaiweb.fairy.utils.tween.SpriteTween;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.Pixmap;

/**
 * The Screen Transition System class using Java Universal Tween Engine.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class ScreenTransition implements Screen
{
    public final static float screenWidth = Gdx.graphics.getWidth();
    public final static float screenHeight = Gdx.graphics.getHeight();
    
    public static final int TOP = 0;
	public static final int BOTTOM = 1;
	public static final int LEFT = 2;
	public static final int RIGHT = 3;
 
    private Game game;
    private Screen current;
    private Screen next;
 
    private FrameBuffer currentBuffer;
    private FrameBuffer nextBuffer;
 
    private SpriteBatch spriteBatch;
    private TweenManager manager;
    private TweenCallback backgroundAnimationTweenComplete;
 
    private Sprite currentScreenSprite;
    private Sprite nextScreenSprite;
    private float nextScreenX = 0;
    private float nextScreenY = 0;
 
    /**
	 * Initialize the transition screen.
	 * 
	 * @param current The current screen
	 * @param next The next screen
	 * @param game The game object
	 * @param origin The origin of the next screen (TOP, BOTTOM, LEFT, RIGHT)
	 */
    public ScreenTransition(Screen current, Screen next, Game game, int origin)
    {
        this.current = current;
        this.next = next;
        this.game = game;
        
        switch(origin){
        	case TOP: nextScreenY = screenHeight; break;
        	case BOTTOM: nextScreenY = -screenHeight; break;
        	case LEFT: nextScreenX = -screenWidth; break;
        	case RIGHT: nextScreenX = screenWidth; break;
        }
    }
 
    @Override
    public void render(float delta)
    {
        manager.update(Gdx.graphics.getDeltaTime());
 
        spriteBatch.begin();
        currentScreenSprite.draw(spriteBatch);
        nextScreenSprite.draw(spriteBatch);
        spriteBatch.end();
 
    }
 
    @Override
    public void show()
    {
		Gdx.input.setInputProcessor(null);
        spriteBatch = new SpriteBatch();
 
        manager = new TweenManager();
        Tween.registerAccessor(Sprite.class, new SpriteTween());
 
        backgroundAnimationTweenComplete = new TweenCallback() {
			@Override
			public void onEvent(int arg0, BaseTween<?> arg1) {
				game.setScreen(next);
			}
        };
 
        nextBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, (int)screenWidth, (int)screenHeight, false);
 
        nextBuffer.begin();
        next.render(Gdx.graphics.getDeltaTime());
        nextBuffer.end();
 
        nextScreenSprite = new Sprite(nextBuffer.getColorBufferTexture());
        nextScreenSprite.setPosition(nextScreenX, nextScreenY);
        nextScreenSprite.flip(false, true);
 
        currentBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, (int)screenWidth, (int)screenHeight, false);
        currentBuffer.begin();
        current.render(Gdx.graphics.getDeltaTime());
        currentBuffer.end();
 
        currentScreenSprite = new Sprite(currentBuffer.getColorBufferTexture());
        currentScreenSprite.setPosition(0, 0);
        currentScreenSprite.flip(false, true);
        
        Tween.to(currentScreenSprite, SpriteTween.XY, 0.4f)
        .target(-nextScreenX, -nextScreenY)
        .ease(Quart.INOUT)
        .start(manager);
        
        Tween.to(nextScreenSprite, SpriteTween.XY, 0.4f)
        .target(0, 0)
        .ease(Quart.INOUT)
        .setCallback(backgroundAnimationTweenComplete)
        .setCallbackTriggers(TweenCallback.COMPLETE)
        .start(manager);
    }
 
    @Override
    public void resume() {
        ;
    }

	@Override
	public void resize(int width, int height) {
		;
	}

	@Override
	public void hide() {
		;
	}

	@Override
	public void pause() {
		;
	}

	@Override
	public void dispose() {
		currentBuffer.dispose();
		nextBuffer.dispose();
	}
}
