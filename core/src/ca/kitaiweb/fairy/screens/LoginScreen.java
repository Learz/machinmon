package ca.kitaiweb.fairy.screens;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import ca.kitaiweb.fairy.FairyGame;
import ca.kitaiweb.fairy.components.*;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

/**
 * The login screen.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class LoginScreen extends ParametrableScreen {

	private Table table;
	private TextButton butt;
	private TextField tfLogin;
	private TextField tfPassword;
	private Label lbLogin;
	private Label lbPassword;
	private Label labl;
	private Label error;
	private Image background;
	
	/**
	 * Initializes the login screen
	 * 
	 * @param fairyGame The game instance in which to display the screen
	 */
	public LoginScreen(FairyGame fairyGame) {
		super(fairyGame);
		background = new Image("background.png");
		stage.addActor(background);
		labl = new Label("Project Fairy", 32);
		error = new Label("", 16);
		error.setAlignment(Align.center);
		butt = new TextButton("Login", 24);
		lbLogin = new Label("Username", 12);
		tfLogin = new TextField("binarmorker", 16);
		lbPassword = new Label("Password", 12);
		tfPassword = new TextField("lmfao1070", 16);
		tfPassword.setPasswordCharacter('*');
		tfPassword.setPasswordMode(true);

		table = new Table();
	    table.setFillParent(true);
		
		table.add(labl).top().colspan(2);
		table.row();
		table.add(error).expand().top().colspan(2);
		table.row();
		table.add(tfLogin).expandX().top();
		table.row();
		table.add(lbLogin).expandX().center();
		table.row();
		table.add(tfPassword).expandX().top();
		table.row();
		table.add(lbPassword).expandX().top();
		table.row();
		table.add(butt).expand().colspan(2).padTop(5).top();
		
		table.pad(10);
		
	    stage.addActor(table);
	    butt.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				try {
					byte[] hash = MessageDigest.getInstance("SHA-256").digest(tfPassword.getText().getBytes("UTF-8"));
					StringBuffer hexString = new StringBuffer();
					for (int i = 0; i < hash.length; i++) {
		                String hex = Integer.toHexString(0xff & hash[i]);
		                if(hex.length() == 1) hexString.append('0');
		                hexString.append(hex);
		            }
					game.authenticateUser(game.mapScreen, ScreenTransition.RIGHT, tfLogin.getText(), hexString.toString(), error);
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
	    });
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	    stage.act(delta);
		stage.draw();
	}
	
	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
	}
	
	@Override
	public void hide() {
		error.setText("");
		tfLogin.setText("");
		tfPassword.setText("");
	}
	
	@Override
	public void resize (int width, int height) {
	    stage.getViewport().update(width, height, true);
	}
	
	@Override
	public void dispose() {
	    stage.dispose();
	}
}
