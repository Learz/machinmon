package ca.kitaiweb.fairy.screens;


import java.util.LinkedList;
import java.util.List;

import ca.kitaiweb.fairy.FairyGame;
import ca.kitaiweb.fairy.components.Dialog;
import ca.kitaiweb.fairy.components.HitPoint;
import ca.kitaiweb.fairy.components.Image;
import ca.kitaiweb.fairy.components.ImageTextButton;
import ca.kitaiweb.fairy.components.Label;
import ca.kitaiweb.fairy.components.TextButton;
import ca.kitaiweb.fairy.components.TextField;
import ca.kitaiweb.fairy.models.FairyAttack;
import ca.kitaiweb.fairy.models.User;
import ca.kitaiweb.fairy.models.UserFairy;
import ca.kitaiweb.fairy.utils.APIRequest;
import ca.kitaiweb.fairy.utils.CombatEngine;
import ca.kitaiweb.fairy.utils.APIRequest.APIRequestType;
import ca.kitaiweb.fairy.utils.CombatEngine.CombatStatus;
import ca.kitaiweb.fairy.utils.PlayerLoader;
import ca.kitaiweb.fairy.utils.PlayerLoader.APIResponseException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.Net.HttpResponseListener;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

/**
 * The combat screen.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class CombatScreen extends ParametrableScreen {
	private Table table;
	private Table visualTable;
	private Table actionsTable;
	private Table attacksTable;
	private Table partyTable;
	private Image background;
	
	private TextButton btnQuit;
	private TextButton btnAttackReturn;
	private TextButton btnAttack;
	private TextButton[] btnAttackList = new TextButton[] {
											new TextButton(""),
											new TextButton(""),
											new TextButton(""),
											new TextButton("")};
	private TextButton btnCapture;
	private TextButton btnPartyReturn;
	private TextButton btnParty;
	private ImageTextButton[] btnFairyList = new ImageTextButton[] {
												new ImageTextButton("slime", "", true),
												new ImageTextButton("slime", "", true),
												new ImageTextButton("slime", "", true)};
	
	private Dialog dlgCombat;
	private Dialog dlgPrompt;
	private Dialog dlgFairyName;
	private TextField tfFairyName;
	
	private Image playerFairyImage;
	private Image opponentFairyImage;
	private CombatEngine combatEngine;
	private User opponent;
	private Label playerFairyName;
	private Label playerFairyLife;
	private Label opponentFairyName;
	private Label opponentFairyLife;
	
	private HitPoint hitPoints;
	
	/**
	 * Initializes the combat screen
	 * 
	 * @param fairyGame The game instance in which to display the screen
	 */
	public CombatScreen(FairyGame fairyGame) {
		super(fairyGame);

		background = new Image("background.png");
		stage.addActor(background);
		playerFairyImage = new Image("slime_back.png");
		opponentFairyImage = new Image("slime.png");
		playerFairyName = new Label("", 16);
		playerFairyLife = new Label("", 24);
		opponentFairyName = new Label("", 16);
		opponentFairyLife = new Label("", 24);
		hitPoints = new HitPoint(playerFairyLife, opponentFairyLife);
		
		btnQuit = new TextButton("Flee", 24);
		btnAttackReturn = new TextButton("Return", 24);
		btnAttack = new TextButton("Attack", 24);
		btnCapture = new TextButton("Capture", 24);
		btnPartyReturn = new TextButton("Return", 24);
		btnParty = new TextButton("Party", 24);
		
		dlgCombat = new Dialog(""){
			protected void result (Object object) {
				combatEngine.saveFairy();
				game.loadUser();
				game.setScreenWithTransition(game.mapScreen, ScreenTransition.BOTTOM);
				dlgCombat.remove();
		    }
		};
		
		dlgPrompt = new Dialog("");
		
		dlgFairyName = new Dialog(""){
			protected void result (Object object) {
				combatEngine.setCapturedFairyName(tfFairyName.getText());
				tfFairyName.setText("");
				combatEngine.capture();
		    }
		};
		
		table = new Table();
		actionsTable = new Table();
		attacksTable = new Table();
		partyTable = new Table();
		visualTable = new Table();
	    table.setFillParent(true);
	    table.add(visualTable).expand().fill().top().uniform();
	    
	    dlgCombat.button(new TextButton("Continue"));
	    dlgCombat.invalidateHierarchy();
	    dlgCombat.invalidate();
	    dlgCombat.layout();
	    
	    dlgPrompt.button(new TextButton("Continue"));
	    dlgPrompt.invalidateHierarchy();
	    dlgPrompt.invalidate();
	    dlgPrompt.layout();
	    
	    dlgFairyName.setTitle("Give this fairy a name :");
	    dlgFairyName.add(tfFairyName = new TextField(""));
	    dlgFairyName.row();
	    dlgFairyName.button(new TextButton("Accept"));
	    dlgFairyName.invalidateHierarchy();
	    dlgFairyName.invalidate();
	    dlgFairyName.layout();
	    
	    Table playerFairyTable = new Table();
	    playerFairyTable.add(playerFairyName).center();
	    playerFairyTable.row();
	    playerFairyTable.add(playerFairyLife).center();
	    
	    Table opponentFairyTable = new Table();
	    opponentFairyTable.add(opponentFairyName).center();
	    opponentFairyTable.row();
	    opponentFairyTable.add(opponentFairyLife).center();
	    
	    visualTable.row().uniform();
	    visualTable.add(opponentFairyTable).center().expand().uniform();
	    visualTable.add(opponentFairyImage).bottom().right().expand().uniform();
	    visualTable.row().uniform();
	    visualTable.add(playerFairyImage).bottom().left().expand().uniform();
	    visualTable.add(playerFairyTable).center().expand().uniform();
	    
	    
	    table.row();
		table.add(actionsTable).expand().fill().bottom().uniform();
		
		actionsTable.add(btnAttack).fill().expand().uniform().pad(5);
		actionsTable.add(btnCapture).fill().expand().uniform().pad(5);
		actionsTable.row();
		actionsTable.add(btnParty).fill().expand().uniform().pad(5);
		actionsTable.add(btnQuit).fill().expand().uniform().pad(5);

		attacksTable.add(btnAttackList[0]).fill().expand().uniform().pad(5);
		attacksTable.add(btnAttackList[1]).fill().expand().uniform().pad(5);
		attacksTable.row();
		attacksTable.add(btnAttackList[2]).fill().expand().uniform().pad(5);
		attacksTable.add(btnAttackList[3]).fill().expand().uniform().pad(5);
		attacksTable.row();
		attacksTable.add(btnAttackReturn).fill().expand().colspan(2).pad(5);
		System.err.println(String.valueOf(this.viewport.getScreenWidth()));
		System.err.println(String.valueOf(this.viewport.getWorldWidth()));
		for(int i = 0; i < btnFairyList.length; i++){
			partyTable.add(btnFairyList[i]).width((this.viewport.getWorldWidth() / btnFairyList.length)-10).expandY().fillY().uniformY().left().pad(5);
		}
		partyTable.row();
		partyTable.add(btnPartyReturn).expandX().fillX().uniformX().colspan(btnFairyList.length).pad(5);
		
	    stage.addActor(table);
		stage.addActor(hitPoints);
	    
	    btnAttack.addListener(new ChangeListener() {
	    	@Override
	    	public void changed(ChangeEvent event, Actor actor) {
	    		table.getCell(actionsTable).setActor(attacksTable);
	    		combatEngine.setStatus(CombatStatus.ATTACKSELECT);
	    	}
	    });
	    
	    for (int i = 0; i < btnAttackList.length; i++) {
	    	final int id = i;
	    	btnAttackList[i].addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					combatEngine.setStatus(CombatStatus.ATTACKING);
					fight(id);
	    			table.getCell(attacksTable).setActor(actionsTable);
				}
	    	});
	    }
	    
	    btnCapture.addListener(new ChangeListener(){
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				capture();
	    		combatEngine.setStatus(CombatStatus.USINGITEM);
			}
		});
	    
	    btnAttackReturn.addListener(new ChangeListener(){
			@Override
			public void changed(ChangeEvent event, Actor actor) {
	    		table.getCell(attacksTable).setActor(actionsTable);
	    		combatEngine.setStatus(CombatStatus.ACTIONSELECT);
			}
		});
	    
	    btnPartyReturn.addListener(new ChangeListener(){
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				table.getCell(partyTable).setActor(actionsTable);
				combatEngine.setStatus(CombatStatus.ACTIONSELECT);
			}
	    });
	    
	    btnParty.addListener(new ChangeListener(){
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				table.getCell(actionsTable).setActor(partyTable);
				combatEngine.setStatus(CombatStatus.FAIRYSELECT);
			}
	    });
	    
	    for (int i = 0; i < btnFairyList.length; i++) {
	    	final int id = i;
	    	btnFairyList[i].addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					if(combatEngine.getStatus() == CombatStatus.FAIRYSELECT){
						changeFairy(id);
					}else if(combatEngine.getStatus() == CombatStatus.SWAPING){
						swapFairy(id);
					}
					combatEngine.setStatus(CombatStatus.WAITING);
					fight(-1);
					combatEngine.setStatus(CombatStatus.ACTIONSELECT);
					table.getCell(partyTable).setActor(actionsTable);
				}
	    	});
	    }
	    
	    btnQuit.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (combatEngine.hasFled()) {
					combatEngine.saveFairy();
					game.loadUser();
					game.setScreenWithTransition(game.mapScreen, ScreenTransition.BOTTOM);
				} else {
					fight(-1);
				}
			}
	    });
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	    stage.act(delta);
		stage.draw();
	}
	
	@Override
	public void show() {
		Gdx.input.setInputProcessor(null);
		game.loadUser();
		if (hasParam("opponent_username")) {
			APIRequest request = new APIRequest("http://protofairy.tk/api/user/get/"+getParam("opponent_username"), APIRequestType.SELECT);
			request.sendRequest(new HttpResponseListener() {
				@Override
				public void handleHttpResponse(HttpResponse httpResponse) {
					try {
						opponent = PlayerLoader.getPlayer(PlayerLoader.parseJson(httpResponse.getResultAsString()), null);
						combatEngine = new CombatEngine(game, opponent);
						setFairies();
					} catch (APIResponseException e) {
						System.err.println(e.getCode()+" -> "+e.getMessage());
					}
				}
				public void failed(Throwable t) {}
				public void cancelled() {}
			});
		} else if (hasParam("fairy_id")) {
			int userLevel = game.getUser().getParty().get(game.getUser().getPrimary()).getLevel() + 1;
			APIRequest request = new APIRequest("http://protofairy.tk/api/fairy/"+getParam("fairy_id")+"/"+userLevel, APIRequestType.SELECT);
			request.sendRequest(new HttpResponseListener() {
				@Override
				public void handleHttpResponse(HttpResponse httpResponse) {
					opponent = new User(0, "the wild", null, null, 0);
					List<UserFairy> party = new LinkedList<UserFairy>();
					try {
						UserFairy fairy = PlayerLoader.getUserFairy(PlayerLoader.parseJson(httpResponse.getResultAsString()));
						party.add(fairy);
						opponent.setParty(party);
						combatEngine = new CombatEngine(game, opponent);
						setFairies();
					} catch (APIResponseException e) {
						System.err.println(e.getCode()+" -> "+e.getMessage());
					}
				}
				public void failed(Throwable t) {}
				public void cancelled() {}
			});
		} 
		Gdx.input.setInputProcessor(stage);
	}
	
	@Override
	public void hide() {
		playerFairyName.setText("");
		playerFairyLife.setText("");
		opponentFairyName.setText("");
		opponentFairyLife.setText("");
		combatEngine.playerFairy.setHealth(combatEngine.playerFairy.getFairy().getMaxHealth());
	}
	
	@Override
	public void resize (int width, int height) {
	    stage.getViewport().update(width, height, true);
	}
	
	@Override
	public void dispose() {
	    stage.dispose();
	}
	
	/**
	 * Prompts the user with a dialog
	 * @param text The text to display
	 */
	public void prompt(String text){
		dlgPrompt.setTitle(text);
		dlgPrompt.show(stage);
	}
	
	/**
	 * Prompts the user with a dialog that changes the screen to the map
	 * @param text The text to display
	 */
	public void endCombatDialog(String text){
		dlgCombat.setTitle(text);
		dlgCombat.show(stage);
	}
	
	/**
	 * Prompts the user with a dialog that changes the screen to the map
	 * @param text The text to display
	 */
	public void setCapturedFairyName(){
		dlgFairyName.show(stage);
	}
	
	/**
	 * Saves the fairy, then swaps it for another one
	 * 
	 * @param id The id of the fairy that will replace the current one
	 */
	private void changeFairy(int id) {
		if (combatEngine.setPlayerFairy(id)) {
			combatEngine.saveFairy();
			setFairies();
		}else{
			dlgPrompt.setTitle("You cannot bring an unconcious fairy into combat!");
			dlgPrompt.show(stage);
		}
	}
	
	/**
	 * Shows the party table
	 */
	public void showParty(CombatStatus status){
		combatEngine.setStatus(status);
		if(combatEngine.getStatus() == CombatStatus.SWAPING){
			game.combatScreen.prompt("You must abandon a fairy.");
		}
		table.getCell(actionsTable).setActor(partyTable);
	}
	
	/**
	 * Swaps a fairy in the party for the captured opponent
	 * @param id
	 */
	private void swapFairy(int id){
		combatEngine.saveFairy();
		combatEngine.swapFairy(id);
	}
	
	/**
	 * Display the life and name of the fairies on the screen and set the text of the current fairy's attacks
	 */
	private void setFairies(){
		Gdx.app.postRunnable(new Runnable() {
			@Override
			public void run(){
				int index = 0;
				
				for(TextButton attack : btnAttackList){
					attack.setText("");
				}
				
				for (FairyAttack attack : combatEngine.playerFairy.getFairy().getAttacks()) {
					btnAttackList[index].setText(attack.getAttack().getName());
					index++;
				}
		
				for (int i = 0; i < game.getUser().getParty().size(); i++) {
					btnFairyList[i].setText(game.getUser().getParty().get(i).getName());
				}
				
				playerFairyName.setText(combatEngine.playerFairy.getName());
				playerFairyLife.setText(String.valueOf(combatEngine.playerFairy.getHealth()));
				opponentFairyName.setText(combatEngine.opponentFairy.getName());
				opponentFairyLife.setText(String.valueOf(combatEngine.opponentFairy.getHealth()));
			}
		});
	}
	
	/**
	 * Set the screen to display the attacks, update health and show hit points
	 * 
	 * @param id The attack id
	 */
	private void fight(int id) {
		Gdx.input.setInputProcessor(null);
		
		if (id >= 0) {
			combatEngine.attack(id);
		}
		
		if (combatEngine.getLastPlayerHit() > 0) {
			opponentFairyName.setText(combatEngine.opponentFairy.getName());
			opponentFairyLife.setText(String.valueOf(combatEngine.opponentFairy.getHealth()));
		} else {
			playerFairyName.setText(combatEngine.playerFairy.getName());
			playerFairyLife.setText(String.valueOf(combatEngine.playerFairy.getHealth()));
		}
		hitPoints.showPlayerHit(combatEngine.getLastPlayerHit(), new Runnable() {
			@Override
			public void run() {
				combatEngine.defend();
				if (combatEngine.getLastOpponentHit() > 0) {
					playerFairyName.setText(combatEngine.playerFairy.getName());
					playerFairyLife.setText(String.valueOf(combatEngine.playerFairy.getHealth()));
				} else {
					opponentFairyName.setText(combatEngine.opponentFairy.getName());
					opponentFairyLife.setText(String.valueOf(combatEngine.opponentFairy.getHealth()));
				}
				
				hitPoints.showOpponentHit(combatEngine.getLastOpponentHit());
				
				/*if (combatEngine.getStatus() == CombatStatus.WON) {
					endCombatDialog("You won!");
				} else if (combatEngine.getStatus() == CombatStatus.LOST) {
					endCombatDialog("You lost...");
				}*/
				
				Gdx.input.setInputProcessor(stage);
			}
		});
	}
	
	/**
	 * set the screen to display the capture of the opponent if succeeded
	 */
	private void capture(){
		if(combatEngine.hasCaptured()){
			setCapturedFairyName();
		}else{
			combatEngine.setStatus(CombatStatus.WAITING);
			fight(-1);
		}
	}
}
