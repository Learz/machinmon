package ca.kitaiweb.fairy.screens;

import ca.kitaiweb.fairy.FairyGame;
import ca.kitaiweb.fairy.components.Image;
import ca.kitaiweb.fairy.components.Label;
import ca.kitaiweb.fairy.components.TextButton;
import ca.kitaiweb.fairy.models.UserFairy;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

/**
 * The fairy description screen
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class FairyScreen extends ParametrableScreen {

	private Table table;
	private TextButton btnQuit;
	private Label lblName;
	private Label lblLevel;
	private Label lblHealth;
	private Label lblDescription;
	private Image background;
	
	/**
	 * Initializes the fairy screen
	 * 
	 * @param fairyGame The game instance in which to display the screen
	 */
	public FairyScreen(FairyGame fairyGame) {
		super(fairyGame);
		background = new Image("background.png");
		stage.addActor(background);
		lblName = new Label("", 32);
		lblLevel = new Label("", 24);
		lblHealth = new Label("", 32);
		lblDescription = new Label("", 24);
		lblDescription.setWrap(true);
		btnQuit = new TextButton("Return", 24);
		
		table = new Table();
	    table.setFillParent(true);
	    
		table.add(lblName).expandY().top();
		table.row();
		table.add(lblLevel).fill().expand().top();
		table.row();
		table.add(lblHealth).fill().expand().top();
		table.row();
		table.add(lblDescription).fill().expand().bottom();
		table.row();
		table.add(btnQuit).expandY().bottom();
		table.pad(10);
		
	    stage.addActor(table);
	    
	    btnQuit.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				game.setScreenWithTransition(game.partyScreen, ScreenTransition.BOTTOM);
			}
	    });
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	    stage.act(delta);
		stage.draw();
	}
	
	@Override
	public void show() {
		UserFairy selection = game.getUser().getParty().get(Integer.parseInt(getParam("id")));
		lblName.setText(selection.getFairy().getId() + " - " + selection.getName());
		lblLevel.setText("Level "+selection.getLevel()+" "+selection.getFairy().getName());
		lblHealth.setText("HP: "+selection.getHealth()+" / "+selection.getFairy().getMaxHealth());
		lblDescription.setText(selection.getFairy().getDescription());
		Gdx.input.setInputProcessor(stage);
	}
	
	@Override
	public void hide() {
		lblName.setText("");
		lblLevel.setText("");
		lblHealth.setText("");
		lblDescription.setText("");
	}
	
	@Override
	public void resize (int width, int height) {
	    stage.getViewport().update(width, height, true);
	}
	
	@Override
	public void dispose() {
	    stage.dispose();
	}
}
