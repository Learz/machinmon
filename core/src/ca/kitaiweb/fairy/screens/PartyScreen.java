package ca.kitaiweb.fairy.screens;

import java.util.HashMap;

import ca.kitaiweb.fairy.FairyGame;
import ca.kitaiweb.fairy.components.Image;
import ca.kitaiweb.fairy.components.ImageButton;
import ca.kitaiweb.fairy.components.Label;
import ca.kitaiweb.fairy.components.ImageTextButton;
import ca.kitaiweb.fairy.models.UserFairy;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

/**
 * The current fairies list screen.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class PartyScreen extends ParametrableScreen {

	private Table table;
	private Table navTable;
	private ImageButton btnMenu;
	private Label lblTitle;
	private Table tableParty;
	private Image background;
	
	/**
	 * Initializes the party screen
	 * 
	 * @param fairyGame The game instance in which to display the screen
	 */
	public PartyScreen(FairyGame fairyGame) {
		super(fairyGame);
		background = new Image("background.png");
		stage.addActor(background);
		lblTitle = new Label("Party", 32);
		btnMenu = new ImageButton("menu", "tab_right");
		tableParty = new Table();
		
		table = new Table();
	    table.setFillParent(true);
		table.add(lblTitle).top();
		table.row();
		table.add(tableParty).expand().fill().top();
		table.pad(10);		
	    stage.addActor(table);
	    
	    navTable = new Table();
	    navTable.setFillParent(true);
	    navTable.add(btnMenu).expand().right();
	    stage.addActor(navTable);
	    
	    btnMenu.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				game.setScreenWithTransition(game.menuScreen, ScreenTransition.RIGHT);
			}
	    });
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	    stage.act(delta);
		stage.draw();
	}
	
	@Override
	public void show(){
		tableParty.clear();
		int index = 0;
		for (final UserFairy item : game.getUser().getParty()) {
			final int i = index;
			ImageTextButton fairy = new ImageTextButton("slime", item.getName(), 24);
			fairy.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					HashMap<String, String> parameters = new HashMap<String, String>();
					parameters.put("id", String.valueOf(i));
					game.setScreenWithTransition(game.fairyScreen, parameters, ScreenTransition.TOP);
				}
			});
			tableParty.add(fairy).expand().fill().uniform().pad(5);
			tableParty.row();
			index++;
		}
		Gdx.input.setInputProcessor(stage);
	}
	
	@Override
	public void hide(){
	}
	
	@Override
	public void resize (int width, int height) {
	    stage.getViewport().update(width, height, true);
	}
	
	@Override
	public void dispose() {
	    stage.dispose();
	}
}

