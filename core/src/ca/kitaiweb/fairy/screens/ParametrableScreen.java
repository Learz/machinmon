package ca.kitaiweb.fairy.screens;

import java.util.HashMap;
import java.util.Map.Entry;

import ca.kitaiweb.fairy.FairyGame;

import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * A screen with transferable parameters
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class ParametrableScreen extends ScreenAdapter {

	FairyGame game;
	protected HashMap<String, String> sashaGrey; // is a HashMap because you can put anything in it
	protected Stage stage;
	protected Viewport viewport;
	
	/**
	 * Pushes the parameters into the HashMap
	 * 
	 * @param params The parameters
	 */
	public void pushParam(HashMap<String, String> params) {
		for (Entry<String, String> param : params.entrySet()) {
			sashaGrey.put(param.getKey(), param.getValue()); // Put a key in Sasha ;)
		}
	}
	
	/**
	 * Pushes a single parameter in the HashMap
	 * 
	 * @param key 
	 * @param value
	 */
	public void pushParam(String key, String value) {
		HashMap<String, String> hashmap = new HashMap<String, String>();
		hashmap.put(key, value);
		pushParam(hashmap);
	}
	
	/**
	 * Get a value from the parameters HashMap
	 * 
	 * @param key The key of the value
	 * @return The value
	 */
	public String getParam(String key) {
		return sashaGrey.get(key); // Get the key back ;)
	}
	
	/**
	 * Checks if the HashMap contains a specific parameter
	 * 
	 * @param key The key of the value
	 * @return If the key exists
	 */
	public boolean hasParam(String key) {
		return sashaGrey.containsKey(key); // She contains many in fact ;)
	}
	
	/**
	 * The constructor
	 * 
	 * @param fairyGame The GDX Game main class
	 */
	public ParametrableScreen(FairyGame fairyGame) {
		super();
		game = fairyGame;
		viewport = new ExtendViewport(FairyGame.SCREENWIDTH, FairyGame.SCREENHEIGHT);
		stage = new Stage(viewport);
		sashaGrey = new HashMap<String, String>();
	}

}
