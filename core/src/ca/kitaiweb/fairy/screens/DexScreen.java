package ca.kitaiweb.fairy.screens;

import java.util.HashMap;

import ca.kitaiweb.fairy.FairyGame;
import ca.kitaiweb.fairy.components.Image;
import ca.kitaiweb.fairy.components.ImageButton;
import ca.kitaiweb.fairy.components.Label;
import ca.kitaiweb.fairy.components.CustomList;
import ca.kitaiweb.fairy.components.ImageTextButton;
import ca.kitaiweb.fairy.models.Encounter;
import ca.kitaiweb.fairy.utils.APIRequest;
import ca.kitaiweb.fairy.utils.APIRequest.APIRequestType;
import ca.kitaiweb.fairy.utils.PlayerLoader.APIResponseException;
import ca.kitaiweb.fairy.utils.PlayerLoader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.Net.HttpResponseListener;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

/**
 * The discovered fairies list screen.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class DexScreen extends ParametrableScreen {

	private Table table;
	private Table navTable;
	private ScrollPane pane;
	private ImageButton btnMap;
	private Label lblTitle;
	private CustomList lstDex;
	private APIRequest request;
	private Image background;
	
	/**
	 * Initializes the fairy list screen
	 * 
	 * @param fairyGame The game instance in which to display the screen
	 */
	public DexScreen(FairyGame fairyGame) {
		super(fairyGame);
		background = new Image("background.png");
		stage.addActor(background);
		lblTitle = new Label("Fairydex", 32);
		btnMap = new ImageButton("map", "tab_left");
		lstDex = new CustomList();
		
		pane = new ScrollPane(lstDex);
		table = new Table();
	    table.setFillParent(true);
		table.add(lblTitle).top();
		table.row();
		table.add(pane).expandY().top();
		table.pad(10);		
	    stage.addActor(table);
	    
	    navTable = new Table();
	    navTable.setFillParent(true);
	    navTable.add(btnMap).expand().left();
	    stage.addActor(navTable);
	    
	    btnMap.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				game.setScreenWithTransition(game.mapScreen, ScreenTransition.LEFT);
			}
	    });
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	    stage.act(delta);
		stage.draw();
	}
	
	@Override
	public void show(){
		request = new APIRequest("http://protofairy.tk/api/user/encounters", APIRequestType.SELECT);
		request.setCredentials(game.getUser().getUsername(), game.getUser().getPassword());
		request.sendRequest(new HttpResponseListener() {
			@Override
			public void handleHttpResponse(HttpResponse httpResponse) {
				final String result = httpResponse.getResultAsString();
				Gdx.app.postRunnable(new Runnable() {
					@Override
					public void run() {
						try {
							lstDex.clear();
							int index = 0;
							for (final Encounter item : PlayerLoader.getEncounters(PlayerLoader.parseJson(result))) {
								final int i = index;
								lstDex.addItem(new ImageTextButton(item.getFairy().getName(), 24), new ChangeListener() {
									@Override
									public void changed(ChangeEvent event, Actor actor) {
										HashMap<String, String> parameters = new HashMap<String, String>();
										parameters.put("id", String.valueOf(i));
										game.setScreenWithTransition(game.encounterScreen, parameters, ScreenTransition.TOP);
									}
								});
								index++;
							}
						} catch (APIResponseException e) {
							System.err.println(e.getCode()+" -> "+e.getMessage());
						}
					}
				});
			}
			public void failed(Throwable t) {}
			public void cancelled() {}
		});
		Gdx.input.setInputProcessor(stage);
	}
	
	@Override
	public void hide(){
	}
	
	@Override
	public void resize (int width, int height) {
	    stage.getViewport().update(width, height, true);
	}
	
	@Override
	public void dispose() {
	    stage.dispose();
	}
}

