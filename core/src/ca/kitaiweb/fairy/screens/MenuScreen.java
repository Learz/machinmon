package ca.kitaiweb.fairy.screens;

import ca.kitaiweb.fairy.FairyGame;
import ca.kitaiweb.fairy.components.Image;
import ca.kitaiweb.fairy.components.ImageButton;
import ca.kitaiweb.fairy.components.Label;
import ca.kitaiweb.fairy.components.TextButton;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

/**
 * The main menu screen.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & François Allard
 */
public class MenuScreen extends ParametrableScreen {

	private Table table;
	private Table navTable;
	private TextButton btnProfile;
	private TextButton btnParty;
	private TextButton btnInventory;
	private TextButton btnOptions;
	private TextButton btnQuit;
	private ImageButton btnMap;
	private Label lblTitle;
	private Image background;
	
	/**
	 * Initializes the menu screen
	 * 
	 * @param fairyGame The game instance in which to display the screen
	 */
	public MenuScreen(FairyGame fairyGame) {
		super(fairyGame);
		background = new Image("background.png");
		stage.addActor(background);
		lblTitle = new Label("Project Fairy", 32);
		btnProfile = new TextButton("Profile", 24);
		btnParty = new TextButton("Party", 24);
		btnInventory = new TextButton("Inventory", 24);
		btnOptions = new TextButton("Options", 24);
		btnQuit = new TextButton("Quit", 24);
		btnMap = new ImageButton("map", "tab_right");
		
		table = new Table();
	    table.setFillParent(true);
		
		table.add(lblTitle).expandY().top();
		table.row();
		table.add(btnProfile).center().pad(5).fill();
		table.row();
		table.add(btnParty).center().pad(5).fill();
		table.row();
		table.add(btnInventory).center().pad(5).fill();
		table.row();
		table.add(btnOptions).center().pad(5).fill();
		table.row();
		table.add(btnQuit).expandY().bottom();
		
		table.pad(10);
		
	    stage.addActor(table);
	    
	    navTable = new Table();
	    navTable.setFillParent(true);
	    
	    navTable.add(btnMap).expand().right();
	    
	    stage.addActor(navTable);
	    
	    btnQuit.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				game.setScreenWithTransition(game.loginScreen, ScreenTransition.BOTTOM);
			}
	    });
	    btnMap.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				game.setScreenWithTransition(game.mapScreen, ScreenTransition.RIGHT);
			}
	    });
	    btnParty.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				game.setScreenWithTransition(game.partyScreen, ScreenTransition.LEFT);
			}
	    });
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	    stage.act(delta);
		stage.draw();
	}
	
	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
	}
	
	@Override
	public void hide() {
	}
	
	@Override
	public void resize (int width, int height) {
	    stage.getViewport().update(width, height, true);
	}
	
	@Override
	public void dispose() {
	    stage.dispose();
	}
}
