package ca.kitaiweb.fairy.android;

import com.badlogic.gdx.backends.android.AndroidApplication;

import ca.kitaiweb.fairy.components.Localizer;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;

public class AndroidLocalizer extends Localizer implements LocationListener {

    private LocationManager locationManager;
    private String provider;
    
    public AndroidLocalizer (AndroidApplication app) {
    	Criteria criteria = new Criteria();
        locationManager = (LocationManager) app.getSystemService(Context.LOCATION_SERVICE);
    	provider = locationManager.getBestProvider(criteria, false);
        //locationManager.requestLocationUpdates(provider, UPDATE_TIME, 0, this);
        Location lastLocation = locationManager.getLastKnownLocation(provider);
        if (lastLocation != null) {
        	setLat(lastLocation.getLatitude());
        	setLon(lastLocation.getLongitude());
        }
    }
    
    @Override
    public void onResume() {
        //locationManager.requestLocationUpdates(provider, UPDATE_TIME, 0, this);
    }
    
    @Override
    public void onPause() {
        //locationManager.removeUpdates(this);
    }
    
    @Override
    public void requestUpdate() {
    	locationManager.requestSingleUpdate(provider, this, Looper.getMainLooper());
    	locationManager.removeUpdates(this);
    }
	
	@Override
	public void onLocationChanged(Location loc) {
		setLat(loc.getLatitude());
		setLon(loc.getLongitude());
	}

	@Override
	public void onProviderDisabled(String arg0) {
		;
	}

	@Override
	public void onProviderEnabled(String arg0) {
		;
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		;
	}

}
