package ca.kitaiweb.fairy.android;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import ca.kitaiweb.fairy.FairyGame;
import ca.kitaiweb.fairy.components.Localizer;
import ca.kitaiweb.fairy.utils.DataProvider;

/**
 * The Android launcher for the libGDX application.
 * 
 * @version 1.0
 * @author Marc-Antoine Renaud & Fran�ois Allard
 */
public class AndroidLauncher extends AndroidApplication {
	
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.useAccelerometer = false;
		config.useCompass = false;
		Localizer localizer = new AndroidLocalizer(this);
		DataProvider randomizer = new RandomDataProvider();
		initialize(new FairyGame(localizer, randomizer, true), config);
	}
}
