Proto Fairy
===========
Proto Fairy is an upcoming ambitious Android game inspired by the popular series Pokémon

About the game
--------------
Proto Fairy is a role-playing alternate reality game in which you capture magical and mythological creatures tu make them battle for you against other creatures, wild or tamed.
Traversing the city, the roads and the forests with your phone in hand, you will encounter special monsters with which you can engage combat when they get close enough. Keep in mind that those do not exist, even though you are moving in the real world!
Rare "raid" creatures will keep you and your group busy fighting while other challengers will make sure you never stop making your pets more powerful.
So, are you ready for the most 1:1 scaled creature capture challenge of the world?

How to play
-----------
The game is currently in closed Beta. If you're willing to try the game and give us useful feedback on your experience, feel free to write your email address at this address: http://protofairy.tk . There will be only a limited number of subscribers who will have the chance to actually download and try the Beta, so don't get your hopes up too much.